# DANIO-CODE Track Hub

This is a project containing all the scripts needed for setting up and
maintenaning the DANIO-CODE Track Hub.

So far it contains a short Python script to create a test Track Hub, and a 
short bash script to create the virtual environment with `trackhub` library 
installed.

To run the `DANIO-CODE_trackhub.py`, firste activate the newly created 
`DANIO-CODE_trackhub_env` by running the following code:

``` source DANIO-CODE_trackhub_env/bin/activate ```

Now in this environment the Python script for creating the Track Hub files can
be run.

The Python script creates the Track Hub which can be uploaded to the genome 
browser by inputing the following URL:

``` https://danio-code.zfin.org/trackhub/DANIO-CODE.hub.txt ```

When run, the following files and folder should be created:

```
DANIO-CODE.hub.txt
DANIO-CODE.genomes.txt
   danRer10/
       trackDb.txt
```
The content of the `DANIO-CODE.hub.txt`:

```
hub DANIO-CODE
shortLabel DANIO-CODE Track Hub
longLabel A Track Hub containing tracks produced with DANIO-CODE pipelines
genomesFile DANIO-CODE.genomes.txt
email daniocode@gmail.com
```
The content of `DANIO-CODE.genomes.txt`:

```
genome danRer10
trackDb danRer10/trackDb.txt
```
The content of `danrer10/trackDb.txt`:

```
track testTract
bigDataUrl http://danio-code.zfin.org/media/trackhub/danRer10/testTrack.bigWig
shortLabel test track
longLabel track for testing purposes
type bigWig
color 128,0,0
```

#!/usr/bin/env python

'''
    A script to create DANIO-CODE Track Hub. It is dependant on 
    DANIO-CODE_tackhub environment.

'''


import trackhub
from trackhub.upload import upload_hub, stage_hub
import os
import glob
from textwrap import dedent
import pandas as pd
from collections import OrderedDict

all_stages = ['1-cell',
              '2-cell',
              '4-cell',
              '16-cell',
              '32-cell',
              '64-cell',
              '128-cell',
              '256-cell',
              '512-cell',
              '1k-cell',
              'High',
              'Oblong',
              'Sphere',
              'Dome',
              '30%-epiboly',
              '50%-epiboly',
              'Germ-ring',
              'Shield',
              '75%-epiboly',
              '90%-epiboly',
              'Bud',
              '1-4 somites',
              '5-9 somites',
              '10-13 somites',
              '14-19 somites',
              '20-25 somites',
              '26+ somites',
              'Prim-5',
              'Prim-15',
              'Prim-25',
              'High-pec',
              'Long-pec',
              'Pec-fin',
              'Protruding-mouth',
              'Day 4',
              'Day 5',
              'Day 6',
              'Days 7-13',
              '90 Days-2 Years',
              'None']
all_stages = [str(s).replace(' ','_') for s in all_stages]
all_stages_enumerated = enumerate(all_stages)
all_stages_enumerated = {j:"_".join([str(i).zfill(2),j]) for i,j in all_stages_enumerated}

DEFAULT_FIELDS=['sequencing__sequencing_id', 
    'sequencing__technicalreplicate__appliedassay__biosamplereplicate__biosample__stage__name', 
    'sequencing__technicalreplicate__appliedassay__assay__assay_lab__name', 
    'sequencing__technicalreplicate__appliedassay__biosamplereplicate__biosample__anatomical_term__name', 
    'sequencing__technicalreplicate__appliedassay__biosamplereplicate__biosample__series__sra_geo_id', 
    'primary_file']

def get_entries(assay, track_types, fields=DEFAULT_FIELDS, **kwargs):
  return Data.objects.filter(sequencing__technicalreplicate__appliedassay__assay__assay_type__name=assay, 
    file_type__name=track_types, **kwargs).values_list(*fields)


# The assay functions

def BScomposite(assay):

  bs_bw = get_entries(assay, 'BIGWIG')
  bs_cpg = glob.glob('/'.join([FILES_BASE, assay, '*/*CpG.bb']))
  bs_chg = glob.glob('/'.join([FILES_BASE, assay, '*/*CHG.bb']))
  bs_chh = glob.glob('/'.join([FILES_BASE, assay, '*/*CHH.bb']))
  bs_stages = []
  bs_seq_ids = []

  bw_tracks = []

  for bw in bs_bw:

    bw = [str(x).replace(' ', '_') for x in bw]
    seq_id, stage, lab, tissue, geo, file_name = bw
    bs_stages.append(stage)
    bs_seq_ids.append(seq_id)

    track = trackhub.Track(
        name=seq_id + "_signal",
        source=file_name,
        short_label=', '.join([assay, stage, lab, seq_id, tissue, geo]),
        visibility='hide',
        tracktype='bigWig',
        viewLimits='0:100',
        color=trackhub.helpers.hex2rgb("#DE2D26"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage, 'context':'all', 'sample':seq_id,'kind':'signal', 'enstage': all_stages_enumerated[stage]},
      )
    bw_tracks.append(track)

  composite = trackhub.CompositeTrack(
    name = 'BSseqComposite',
    short_label='BS-seq tracks',
    tracktype='bed 3',
    sortOrder='enstage=+ sample=+',
    dimensions="dimX=stage dimY=context dimA=kind dimB=sample dimC=enstage",
    filterComposite="dimA dimB dimC"
    )
  
  subgroup_stages = [stage for stage in all_stages if stage in bs_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  context_mapping = OrderedDict([
            ('CpG', 'CpG'),
            ('CHG', 'CHG'),
            ('CHH', 'CHH'),
            ('all', 'all'),
        ]) 
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in bs_stages}   
  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track
    ),
    trackhub.SubGroupDefinition(
        name='context',
        label='Context',
        mapping=context_mapping,          
     ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Kind',
        mapping={'signal':'signal','region':'region'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sample',
      mapping=dict(zip(bs_seq_ids, bs_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      color=trackhub.helpers.hex2rgb("#DE2D26"),
      short_label='Methylation signal')

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='region',
      visibility='dense',
      tracktype='bigBed 9 +',
      short_label='Methylated Cs')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)

  def region_tracks(context, tracks):

    context_tracks = []

    for track in tracks:

      seq_id = track.split('.')[1]
      stage = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0].replace(' ', '_')
      lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
      tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
      geo =  str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')


      track_object = trackhub.Track(
        name=context + "_" + seq_id,
        source=track,
        long_label=', '.join([context, stage, lab, tissue, geo]),
        tracktype='bigBed 9 +',
        visibility='dense',
        itemRgb='on',
        subgroups={'stage':stage, 'context': context, 'kind':'region', 'sample':seq_id, 'enstage': all_stages_enumerated[stage]},
      )
      
      context_tracks.append(track_object)

    return(context_tracks)

  regions_view.add_tracks(region_tracks('CpG', bs_cpg))
  regions_view.add_tracks(region_tracks('CHG', bs_chg))
  regions_view.add_tracks(region_tracks('CHH', bs_chh))

  return composite

def ATACcomposite(assay):

  atac_bw = get_entries(assay, 'BIGWIG')
  # have to import the files!!!
  atac_bnw = get_entries(assay, 'BIGNARROWPEAK')
  atac_stages = []
  atac_seq_ids = []
  bw_tracks = []

  for bw in atac_bw:

    bw = [str(x).replace(' ', '_') for x in bw]
    seq_id, stage, lab, tissue, geo, file_name = bw

    atac_stages.append(stage)
    atac_seq_ids.append(seq_id)

    if stage == 'Prim-5':
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name=seq_id+"_signal",
        long_label=', '.join([stage, lab, tissue, geo]),
        source=file_name,
        visibility=visibility,
        tracktype='bigWig',
        autoScale='on',
        color=trackhub.helpers.hex2rgb("#238B45"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage, 'kind':'signal', 'sample':seq_id, 'enstage': all_stages_enumerated[stage]},
      )
    bw_tracks.append(track)

  region_tracks = []

  for track in atac_bnw:

    track = [str(x).replace(' ', '_') for x in track]
    seq_id, stage, lab, tissue, geo, file_name = track

    if stage == 'Prim-5':
      visibility = 'dense'
    else:
      visibility = 'hide'


    atac_seq_ids.append(seq_id)

    track_object = trackhub.Track(
      name=seq_id + '_region',
      long_label = ', '.join([stage, lab, tissue, geo]),
      source=file_name,
      tracktype='bigBed 6 4',
      visibility=visibility,
      itemRgb='on',
      subgroups={'stage':stage, 'kind':'region', 'sample':seq_id, 'enstage':all_stages_enumerated[stage]},
    )
      
    region_tracks.append(track_object)
  
  composite = trackhub.CompositeTrack(
    name = 'ATAC-seqComposite',
    short_label='ATAC-seq tracks',
    tracktype='bed 3',
    sortOrder="enstage=+ sample=+ kind=-",
    dimensions="dimX=stage dimA=kind dimB=sample dimC=enstage",
    filterComposite="dimA dimB",
    html_string=dedent(
    """
    ATAC-seq tracks
    ---------------

    This is an ATAC-seq super track that groups together signal of ATAC-seq data (bigWig format) together with the narrow peak signal (bigNarrowPeak format).
    regions (bigBed format).

    The ATAC-seq pipeline used in the analysis is an adapted 
    ATAC-Seq / DNase-Seq Pipeline developed at Kundaje lab 
    (https://github.com/kundajelab/atac_dnase_pipelines). The pipeline consists of 
    the following steps:


    - Quality filtering and trimming of raw reads (Trim-galore)
    - Mapping reads to the reference genome (Bowtie2)
    - Filtering of reads in the output bam file (samtools and sambamba)
    - Marking optical duplicates (Picard tools)
    - Deduplication (samtools and sambamba)
    - Format conversion to a more manageable tagAlign format (https://genome.ucsc.edu/FAQ/FAQformat.html#format15)
    - Tn5 shifting of aligned reads
    - Peak calling (Macs2)

    The analysis also include fragment length distribution of aligned read pairs,
    which allows separation of reads into nucleosome-free region spanning, 
    mononucleosome spanning, and di and more nucleosome spanning.

    **Track definition**:
        ATAC-seq tracks consist of 2 parts:
        #. ATAC-seq signal.
        #. Narrow peak signals.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ATAC-seq>`.

    """
      )
    )
  
  subgroup_stages = [stage for stage in all_stages if stage in atac_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in atac_stages}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track kind',
        mapping={'signal':'signal','region':'region'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(atac_seq_ids, atac_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerate_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      tracktype='bigWig',
      short_label='ATAC-seq Signals',
      color=trackhub.helpers.hex2rgb("#238B45"))

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='regions',
      tracktype='bigBed 6 4',
      short_label='ATAC-seq peaks')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)
  regions_view.add_tracks(region_tracks)

  return composite

def RNAcomposite(assay):

  neg = glob.glob('/'.join([FILES_BASE, assay, "*/*.neg.bw"]))
  pos = glob.glob('/'.join([FILES_BASE, assay,"*/*.pos.bw"]))

  neg_files = {}

  for f in neg:
    seq_id = f.split('.')[1]
    neg_files[seq_id] = f
  
  unstrand = {}
  pos_files = {}

  stranded_seqids = neg_files.keys()
  rna_stages = {}

  for f in pos:
    seq_id = f.split('.')[1]
    stage = str( dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    rna_stages[seq_id] = stage
    if seq_id in stranded_seqids:
        pos_files[seq_id] = f
    else:
        unstrand[seq_id] = f 

  composite = trackhub.CompositeTrack(
    name = 'RNA-seqComposite',
    short_label='RNA-seq tracks',
    tracktype='bigWig',
    sortOrder="enstage=+ sample=+ strand=-",
    dimensions="dimX=stage dimA=strand dimB=sample dimC=enstage",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    RNA-seq tracks
    ---------------

    This is a RNA-seq super track that groups together signal of RNA-seq data (bigWig format) of stranded and unstranded RNA-seq samples.
    regions (bigBed format).

    The RNA-seq pipeline used to generate these tracks are described `here <https://gitlab.com/danio-code/DANIO-CODE_RNA-seq>`.

    Briefly, the pipeline consists of:
      #. Reference genome index generation (one time only).
      #. Alignment files with STAR: The part of the pipeline which maps the reads to the reference genome.
      #. Quality control of alignment: The second step which controls the quality of the alignments from STAR.
      #. bedGraph to bigWig signal conversion.

    **Track definition**:
        RNA-seq tracks consist of 2 different types:
        #. RNA-seq signals for stranded samples, which consist of positive and negative strands.
        #. RNA-seq signals for stranded samples, which consist of collapsed positive strands.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ADANIO-CODE_RNA-seq>`.

    """
      ))

  subgroup_stages = [stage for stage in all_stages if stage in rna_stages.values()]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in rna_stages.values()}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
      name='strand',
      label='Strand',
      mapping={'pos':'+', 'neg':'-', 'uns':'unstranded'}
      ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(stranded_seqids + unstrand.keys(), stranded_seqids + unstrand.keys()))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  view = trackhub.ViewTrack(
      name='Track_view',
      view='signal',
      short_label='RNA-seq signal')

  #unstranded_view = trackhub.ViewTrack(
  #    name='signalviewtrack',
  #    view='usignal',
  #    visibility='full',
  #    tracktype = 'bigWig',
  #    short_label='Unstranded Signal')

  composite.add_subgroups(subgroups)
  composite.add_view(view)
  #composite.add_view(unstranded_view)

  track_list = []

  for sample in neg_files.keys():
    lab = dcc_data.loc[dcc_data['sequencing_id'] == sample, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == sample, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == sample, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    if rna_stages[sample] == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    pos_track = trackhub.Track(
      name=sample + '_pos',
      long_label=', '.join([rna_stages[sample], lab, tissue, geo]),
      source=pos_files[sample],
      tracktype='bigWig',
      visibility=visibility,
      autoScale="on",
      maxHeightPixels='128:32:8',
      color = trackhub.helpers.hex2rgb('#FF0000'),
      subgroups={'stage':rna_stages[sample], 'sample':sample, 'strand':'pos', 'enstage': all_stages_enumerated[rna_stages[sample]]},
    )

    neg_track = trackhub.Track(
      name=sample + '_neg',
      long_label=', '.join([rna_stages[sample], lab, tissue, geo]),
      source=neg_files[sample],
      tracktype='bigWig',
      visibility = visibility,
      autoScale="on",
      maxHeightPixels='128:32:8',
      color = trackhub.helpers.hex2rgb('#0000FF'),
      subgroups={'stage':rna_stages[sample], 'sample':sample, 'strand':'neg', 'enstage': all_stages_enumerated[rna_stages[sample]]},
    )

    track_list.append(pos_track)
    track_list.append(neg_track)

    #overlay = trackhub.AggregateTrack(
    #  aggregate='transparentOverlay',
    #  visibility='full',
    #  tracktype='bigWig',
    #  viewLimits='0:100',
    #  maxHeightPixels='128:32:8',
    #  showSubtrackColorOnUi='on',
    #  name='Stranded' + sample)

    #overlay.add_subtrack(pos_track)
    #overlay.add_subtrack(neg_track)
    #agg_tracks.append(overlay)

  #stranded_view.add_tracks(agg_tracks)
  
  unstranded_tracks = []

  for seq_id, file in unstrand.items():
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    if rna_stages[sample] == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name=seq_id + "_unstranded",
        long_label=', '.join([rna_stages[seq_id], lab, tissue, geo]),
        source=file,
        visibility=visibility,
        tracktype='bigWig',
        autoScale='on',
        maxHeightPixels='128:32:8',
        color=trackhub.helpers.hex2rgb('#7a0177'),
        subgroups={'stage':rna_stages[seq_id], 'sample':seq_id, 'strand':'uns', 'enstage': all_stages_enumerated[rna_stages[seq_id]]},
      ) 

    track_list.append(track)

  view.add_tracks(track_list)

  #unstranded_view.add_tracks(unstranded_tracks)

  return composite


def CAGEcomposite(assay):

  cage_bw = get_entries(assay, 'BIGWIG')
  cage_bb = get_entries(assay, 'BB')

  cage_stages = []
  cage_seq_ids = []
  bw_tracks = []

  for bw in cage_bw:
    
    bw = [str(x).replace(' ', '_') for x in bw]
    seq_id, stage, lab, tissue, geo, file_name = bw

    cage_stages.append(stage)
    cage_seq_ids.append(seq_id)

    if stage == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name=seq_id + "_signal",
        long_label=', '.join([stage, lab, tissue, geo]),
        source=file_name,
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        maxHeightPixels='128:32:8',
        color=trackhub.helpers.hex2rgb("#9ECAE1"),
        subgroups={'stage':stage, 'sample':seq_id, 'kind':'signal', 'enstage':all_stages_enumerated[stage]},
      )
    bw_tracks.append(track)

  region_tracks = []

  for track in cage_bb:

    track = [str(x).replace(' ', '_') for x in track]
    seq_id, stage, lab, tissue, geo, file_name = track
    cage_seq_ids.append(seq_id)

    if stage == "Prim-5":
      visibility = 'dense'
    else:
      visibility = 'hide'

    track_object = trackhub.Track(
      name=seq_id + '_tagCluster',
      long_label=', '.join([stage, lab, tissue, geo]),
      source=file_name,
      tracktype='bigBed',
      visibility='dense',
      itemRgb='on',
      subgroups={'stage':stage, 'sample':seq_id, 'kind':'region', 'enstage':all_stages_enumerated[stage]},
    )
      
    region_tracks.append(track_object)
  
  composite = trackhub.CompositeTrack(
    name = 'CAGE-seqComposite',
    short_label='CAGE-seq tracks',
    tracktype='bed 3',
    dimensions="dimX=stage dimA=kind dimB=sample dimC=enstage",
    sortOrder="enstage=+ sample=+ kind=-",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    CAGE-seq tracks
    ---------------

    This is a CAGE-seq super track that groups together signal (bigWig format) and
    tag cluster regions (bigBed format). These tracks were produced by the DANIO-CODE CAGE-seq pipeline.
    
    The main processing pipeline of the DCC ("CAGE_pipeline_v1.6") consists of 4 different processing steps :
      #.Sequence quality filtering / trimming (FastQC)
      #.Mapping to the reference genome (Bowtie)
      #.Tag clustering (Paraclu-9)

    There is also an additional signal conversion step for reformatting the input from the main pipeline 
    to adequate UCSC track input format (DANIO-CODE CAGE-seq signal conversion pipeline v1.0).
    
    **Track definition**:
        CAGE-seq tracks consist of 2 parts:
        #. CAGE-seq TSS signals
        #. and CAGE-seq tag clusters
    
    For more information on how the data were processed, please refer `here <https://gitlab.com/da-bar/DANIO-CODE_CAGE-seq>`.

    """
      )
    )

  subgroup_stages = [stage for stage in all_stages if stage in cage_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in cage_stages}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track kind',
        mapping={'signal':'signal','region':'tc'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(cage_seq_ids, cage_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='regions',
      visibility='dense',
      tracktype='bigBed',
      short_label='Regions')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)  
  regions_view.add_tracks(region_tracks)

  return composite

def ChIPcomposite(assay):

  signals = glob.glob('/zfin/danio-code/non_annotated_files/ChIP-tracks/signals/*.bw')
  peaks = glob.glob('/zfin/danio-code/non_annotated_files/ChIP-tracks/peaks/*.bigNarrowPeak')
  
  chip_stages = []
  targets = []
  signal_tracks = []
  chip_seq_ids = []

  for bw in signals:

    file_name = bw.split('/')[-1]
    seq_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    target = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'target'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    chip_stages.append(stage)
    chip_seq_ids.append(seq_id)
    targets.append(target)
    
    if stage == 'Prim-5':
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name=target + "_" + seq_id + "_signal",
        long_label=', '.join([target, stage, lab, tissue]),
        source=bw,
        visibility=visibility,
        tracktype='bigWig',
        autoScale='on',
        color=trackhub.helpers.hex2rgb("#74C476"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage,'target':target,'kind':'signal', 'sample':seq_id, 'enstage':all_stages_enumerated[stage]},
      )

    signal_tracks.append(track)

  peak_tracks = []

  for bnp in peaks:

    file_name = bnp.split('/')[-1]
    seq_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    target = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'target'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    chip_seq_ids.append(seq_id)

    if stage == 'Prim-5':
      visibility = 'dense'
    else:
      visibility = 'hide'
    
    track = trackhub.Track(
        name=target + "_" + seq_id + "_peak",
        short_label = ', '.join([target, stage, lab, tissue, geo]),
        source=bnp,
        visibility=visibility,
        tracktype='bigBed 6 4',
        autoScale='on',
        itemRgb='on',
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage,'target':target,'kind':'peak','sample':seq_id, 'enstage':all_stages_enumerated[stage]},
      )

    peak_tracks.append(track)
  
  composite = trackhub.CompositeTrack(
    name = 'ChIP-seqComposite',
    short_label='ChIP-seq tracks',
    tracktype='bed 3',
    sortOrder='enstage=+ sample=+ kind=-',
    dimensions="dimX=stage dimY=target dimA=kind dimB=sample dimC=enstage",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    ChIP-seq tracks
    ---------------

    This is a ChIP-seq super track that groups together signal (bigWig format) and narrow peaks (bigNarrowPeak format) of ChIP-seq data.
    regions (bigBed format).

    The ChIP-seq pipeline used to generate these tracks are described `here <https://gitlab.com/danio-code/DANIO-CODE_ChIP-seq>`.

    Briefly, the pipeline consists of:
      #. Aligning reads (bwa): Aligning raw reads to the reference genome.
      #. Filtering of aligned reads: Filtering the ba file for unaligned reads and and duplicates with samtools and sambamba.
      #. Optical duplicates detection: Detect optical duplicates with Picard tools.
      #. Duplicates removal: Second round of filtering with samtools and sambamba.
      #. Cross-correlation analysis.

    **Track definition**:
        ChIP-seq tracks consist of 2 different types:
        #. ChIP-seq signals.
        #. ChIP-seq narrow peaks.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ADANIO-CODE_RNA-seq>`.

    """
      ))

  subgroup_stages = [stage for stage in all_stages if stage in chip_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in chip_stages}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
        name='target',
        label='ChIP-seq_target',
        mapping=dict(zip(targets, targets))
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track_kind',
        mapping={'signal':'signal', 'peak':'peak'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(chip_seq_ids, chip_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')

  peak_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='peaks',
      visibility='dense',
      tracktype='bigBed 6 4',
      short_label='Peaks')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(peak_view)
  
  signal_view.add_tracks(signal_tracks)  
  peak_view.add_tracks(peak_tracks)

  return composite

def MNaseComposite(assay):

  signals = glob.glob('/zfin/danio-code/non_annotated_files/MNase-seq_tracks/*.bigwig')

  mnase_stages = []
  signal_tracks = []
  mnase_bios_ids = []

  for bw in signals:

    file_name = bw.split('/')[-1]
    bios_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    mnase_stages.append(stage)
    
    track = trackhub.Track(
        name=bios_id + "_signal",
        short_label=', '.join([stage, lab, tissue, geo]),
        source=bw,
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        color=trackhub.helpers.hex2rgb("#BAE4B3"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage},
      )

    signal_tracks.append(track)

  composite = trackhub.CompositeTrack(
    name = 'MNase-seqComposite',
    short_label='MNase-seq tracks',
    tracktype='bigWig',
    dimensions="dimX=stage",
    html_string=dedent(
      """
      MNase-seq tracks consist of bigWig files containing negative logarithm of p-value representing the probability of observed signal over expected.
      """
      ))

  subgroup_stages = [stage for stage in all_stages if stage in mnase_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')
  
  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  
  signal_view.add_tracks(signal_tracks)  

  return composite



# This will change when we will set up a new hub on a different server

FILES_BASE = '/zfin/danio-code/annotated_files'
TRACKHUB_LOC = '/zfin/danio-code/trackhub/'
GENOME = 'danRer10'
ASSAYS = ["BS-seq", "ATAC-seq", "RNA-seq", "CAGE-seq", "ChIP-seq", "MNase-seq"]
ASSAY_HUB={"BS-seq":BScomposite, "ATAC-seq":ATACcomposite, "RNA-seq": RNAcomposite, "CAGE-seq":CAGEcomposite, "ChIP-seq": ChIPcomposite, "MNase-seq":MNaseComposite}

# Here we will create a new hub object

hub, genomes_file, genome, trackdb = trackhub.default_hub(
	hub_name='DANIO-CODE', # I think this is enough for the name, I suppose
	short_label='DANIO-CODE Track Hub',
	long_label='A Track Hub containing tracks produced with DANIO-CODE pipelines',
	email='daniocode@gmail.com',
	genome=GENOME
	)

#hub.url =  "https://danio-code.zfin.org/trackhub/DANIO-CODE.hub.txt" DEPRACTED
#hub.remote_fn = "/zfin/danio-code/trackhub/DANIO-CODE.hub.txt"

assert trackdb is hub.genomes_file.genomes[0].trackdb

#track = Track(
#   name='testTrack',
#   tracktype='bigWig',
#   short_label='test track',
#   long_label='track for testing purposes',
#   source='ATAC_test_1M_atac.bam_nodup.monoNsome.bw',
#   color='128,0,0'
#	)

stages_dict = dict(zip(dcc_data.stage.unique(), dcc_data.stage.unique()))

for assay in ASSAYS:

  trackdb.add_tracks(ASSAY_HUB[assay](assay))

#results = hub.render()

kwargs = dict(host = 'localhost', remote_dir = 'test_dir')
#upload_hub(hub=hub, **kwargs)
stage_hub(hub=hub, staging=TRACKHUB_LOC)
#upload_track(track=track, **kwargs)
	
print 'Rendered!'
print 'Finished!'


  



    




#!/bin/bash

# A simple script for creating the DANIO-CODE_trackhub_env
# This script assumes virtaulenv is installed on the system
# For now it also assumes that is run from the TrackHub folder

virtualenv DANIO-CODE_trackhub_env
source DANIO-CODE_trackhub_env/bin/activate
pip install trackhub
pip install pandas

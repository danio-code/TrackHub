import json
import glob
import os

datahub = []
colors = ["#e64b35",
          "#4dbbd6",
          "#02a087",
          "#3C5488",
          "#F39B7F",
          "#8491b4",
          "#dc0000",
          "#91d0c2",
          "#7e6048",
          "#b19c85",
          "#e64b35",
          "#4dbbd6",
          ]

# assays = ["ATAC", "Methyl", "H3K27AC", "RNA"]
assays = ["RNA"]
for assay in assays:
    # bigwigs which were only horizontally shifted
    with open('shifted/'+assay+'/log.json') as f:
        data = json.load(f)
    bws = []
    for i, bw in enumerate(data):
        if bw['assay']=='RNA_neg':
            continue
        stage = bw['stage']
        track = {
            "type": 'bigwig',
            "name": stage,
            "url": 'https://export.uppmax.uu.se/uppstore2017255/trackhub/'+ bw['new_file'],
            "options": {
                "color": colors[i]
            },
            "metadata": {
                "stage": stage,
                "assay": bw['assay']
            }
            }
        bws.append(track)

    datahub.append({
        "type": "matplot",
        "name": bw['assay'],
        "tracks": bws,
        "options": {
            "height": 120
        },
        "metadata": {
            "assay": bw['assay']
        }
        }
    )

    # bigwigs = glob.glob("/proj/uppstore2017255/webexport/trackhub/non_shifted/"+ assay +"/*.bigwig")
    # bigwigs.extend(glob.glob("/proj/uppstore2017255/webexport/trackhub/non_shifted/"+ assay +"/*.bw"))
    # bigwigs = [i for i in bigwigs if not '.neg.' in i]
    # bigwigs.sort(reverse=True)


    for i, bw in enumerate(data):
        datahub.append({
            "type": 'bigwig',
            "name": bw['stage'],
            "url": 'https://export.uppmax.uu.se/uppstore2017255/trackhub/'+ bw['og_file'],
            "options": {
                "color": colors[i]
            },
            "metadata": {
                "stage": bw['stage'],
                "assay": bw['assay'],
                "displayMode": "heatmap" if assay == "H3K27AC" else "bar"
            }
            })

outfile = open('/proj/uppstore2017255/webexport/trackhub/datahub.json', 'w')
datahub = json.dumps(datahub, indent=4)
outfile.write(datahub)

outfile.close()

#!/usr/bin/env python

'''
    A script to create DANIO-CODE Track Hub. It is dependant on 
    DANIO-CODE_tackhub environment.

'''


import trackhub
from trackhub.upload import upload_hub, stage_hub
import os
import glob
from textwrap import dedent
import pandas as pd

dcc_data = pd.read_csv('annotated_data_May_01_2018.csv')
all_stages = [str(s).replace(' ','_') for s in dcc_data.stage.unique()]

# The assay functions

def BScomposite(assay):

  bs_bw = glob.glob('/'.join([FILES_BASE, assay, '*/*.bw']))
  bs_cpg = glob.glob('/'.join([FILES_BASE, assay, '*/*CpG.bb']))
  bs_chg = glob.glob('/'.join([FILES_BASE, assay, '*/*CHG.bb']))
  bs_chh = glob.glob('/'.join([FILES_BASE, assay, '*/*CHH.bb']))
  bs_stages = []
  bs_seq_ids = []

  bw_tracks = []

  for bw in bs_bw:

    seq_id = bw.split('.')[1]
    stage = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0].replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    bs_stages.append(stage)
    bs_seq_ids.append(seq_id)

    track = trackhub.Track(
        name="Signal"+seq_id,
        source=bw,
        short_label=', '.join([stage, lab, seq_id, tissue]),
        visibility='full',
        tracktype='bigWig',
        viewLimits='0:100',
        color=trackhub.helpers.hex2rgb("#DE2D26"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage, 'sample':seq_id,'kind':'signal'},
      )
    bw_tracks.append(track)

  composite = trackhub.CompositeTrack(
    name = 'BSseqComposite',
    short_label='DANIO-CODE BS-seq tracks',
    tracktype='bed 3',
    sortOrder='sample=+ stage=+',
    dimensions="dimX=stage dimY=context dimA=kind dimB=sample",
    )

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=dict(zip(bs_stages, bs_stages))
    ),
    trackhub.SubGroupDefinition(
        name='context',
        label='Context',
        mapping={
            'CpG': 'CpG',
            'CHG': 'CHG',
            'CHH': 'CHH',
        }              
     ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Kind',
        mapping={'signal':'signal','region':'region'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sample',
      mapping=dict(zip(bs_seq_ids, bs_seq_ids))
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      color=trackhub.helpers.hex2rgb("#DE2D26"),
      short_label='Signal')

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='regions',
      visibility='dense',
      tracktype='bigBed 9 +',
      short_label='Regions')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)

  def region_tracks(context, tracks):

    context_tracks = []

    for track in tracks:

      seq_id = track.split('.')[1]
      stage = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0].replace(' ', '_')
      lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
      tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')

      track_object = trackhub.Track(
        name=context + seq_id,
        source=track,
        short_label=', '.join([stage, context,lab, seq_id, tissue]),
        tracktype='bigBed 9 +',
        visibility='dense',
        itemRgb='on',
        subgroups={'stage':stage, 'context': context, 'kind':'region', 'sample':seq_id},
      )
      
      context_tracks.append(track_object)

    return(context_tracks)

  regions_view.add_tracks(region_tracks('CpG', bs_cpg))
  regions_view.add_tracks(region_tracks('CHG', bs_chg))
  regions_view.add_tracks(region_tracks('CHH', bs_chh))

  return composite

def ATACcomposite(assay):

  atac_bw = glob.glob('/'.join([FILES_BASE, assay, '*/*.pval.signal.bigwig']))
  # have to import the files!!!
  atac_bnw = glob.glob('/zfin/danio-code/non_annotated_files/atac_seq_results/bigNarrowPeaks/*.bigNarrowPeak')
  atac_stages = []
  atac_seq_ids = []
  bw_tracks = []

  for bw in atac_bw:

    seq_id = bw.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')

    atac_stages.append(stage)
    atac_seq_ids.append(seq_id)

    track = trackhub.Track(
        name=seq_id+"_signal",
        short_label=', '.join([stage, lab, seq_id, tissue]),
        source=bw,
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        color=trackhub.helpers.hex2rgb("#238B45"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage, 'kind':'signal', 'sample':seq_id},
      )
    bw_tracks.append(track)

  region_tracks = []

  for track in atac_bnw:

    seq_id = track.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0])
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')

    atac_seq_ids.append(seq_id)

    track_object = trackhub.Track(
      name=seq_id + '_region',
      short_label = ', '.join([stage, lab, seq_id, tissue]),
      source=track,
      tracktype='bigBed 6 4',
      visibility='dense',
      itemRgb='on',
      subgroups={'stage':stage, 'kind':'region', 'sample':seq_id},
    )
      
    region_tracks.append(track_object)
  
  composite = trackhub.CompositeTrack(
    name = 'ATAC-seqComposite',
    short_label='ATAC-seq tracks',
    tracktype='bed 3',
    sortOrder="sample=+ kind=- stage=+",
    dimensions="dimX=stage dimA=kind dimB=sample",
    html_string=dedent(
    """
    ATAC-seq tracks
    ---------------

    This is an ATAC-seq super track that groups together signal of ATAC-seq data (bigWig format) together with the narrow peak signal (bigNarrowPeak format).
    regions (bigBed format).

    The ATAC-seq pipeline used in the analysis is an adapted 
    ATAC-Seq / DNase-Seq Pipeline developed at Kundaje lab 
    (https://github.com/kundajelab/atac_dnase_pipelines). The pipeline consists of 
    the following steps:


    - Quality filtering and trimming of raw reads (Trim-galore)
    - Mapping reads to the reference genome (Bowtie2)
    - Filtering of reads in the output bam file (samtools and sambamba)
    - Marking optical duplicates (Picard tools)
    - Deduplication (samtools and sambamba)
    - Format conversion to a more manageable tagAlign format (https://genome.ucsc.edu/FAQ/FAQformat.html#format15)
    - Tn5 shifting of aligned reads
    - Peak calling (Macs2)

    The analysis also include fragment length distribution of aligned read pairs,
    which allows separation of reads into nucleosome-free region spanning, 
    mononucleosome spanning, and di and more nucleosome spanning.

    **Track definition**:
        ATAC-seq tracks consist of 2 parts:
        #. ATAC-seq signal.
        #. Narrow peak signals.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ATAC-seq>`.

    """
      )
    )

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=dict(zip(atac_stages, atac_stages))
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track kind',
        mapping={'signal':'signal','region':'region'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(atac_seq_ids, atac_seq_ids))
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal',
      color=trackhub.helpers.hex2rgb("#238B45"))

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='regions',
      visibility='dense',
      tracktype='bigBed 6 4',
      short_label='Regions')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)
  regions_view.add_tracks(region_tracks)

  return composite

def RNAcomposite(assay):

  neg = glob.glob('/'.join([FILES_BASE, assay, "*/*.neg.bw"]))
  pos = glob.glob('/'.join([FILES_BASE, assay,"*/*.pos.bw"]))

  neg_files = {}

  for f in neg:
    seq_id = f.split('.')[1]
    neg_files[seq_id] = f
  
  unstrand = {}
  pos_files = {}

  stranded_seqids = neg_files.keys()
  rna_stages = {}

  for f in pos:
    seq_id = f.split('.')[1]
    stage = str( dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    rna_stages[seq_id] = stage
    if seq_id in stranded_seqids:
        pos_files[seq_id] = f
    else:
        unstrand[seq_id] = f 

  composite = trackhub.CompositeTrack(
    name = 'RNA-seqComposite',
    short_label='RNA-seq tracks',
    tracktype='bigWig',
    sortOrder="sample=+ strand=- stage=+",
    dimensions="dimX=stage dimA=strand dimB=sample",
    html_string=dedent(
    """
    RNA-seq tracks
    ---------------

    This is a RNA-seq super track that groups together signal of RNA-seq data (bigWig format) of stranded and unstranded RNA-seq samples.
    regions (bigBed format).

    The RNA-seq pipeline used to generate these tracks are described `here <https://gitlab.com/danio-code/DANIO-CODE_RNA-seq>`.

    Briefly, the pipeline consists of:
      #. Reference genome index generation (one time only).
      #. Alignment files with STAR: The part of the pipeline which maps the reads to the reference genome.
      #. Quality control of alignment: The second step which controls the quality of the alignments from STAR.
      #. bedGraph to bigWig signal conversion.

    **Track definition**:
        RNA-seq tracks consist of 2 different types:
        #. RNA-seq signals for stranded samples, which consist of positive and negative strands.
        #. RNA-seq signals for stranded samples, which consist of collapsed positive strands.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ADANIO-CODE_RNA-seq>`.

    """
      ))

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=dict(zip(rna_stages.values(), rna_stages.values()))
    ),
    trackhub.SubGroupDefinition(
      name='strand',
      label='Strand',
      mapping={'pos':'+', 'neg':'-', 'uns':'unstranded'}
      ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(stranded_seqids + unstrand.keys(), stranded_seqids + unstrand.keys()))
    ),
  ]

  view = trackhub.ViewTrack(
      name='Track_view',
      view='signal',
      visibility='full',
      short_label='RNA-seq signal')

  #unstranded_view = trackhub.ViewTrack(
  #    name='signalviewtrack',
  #    view='usignal',
  #    visibility='full',
  #    tracktype = 'bigWig',
  #    short_label='Unstranded Signal')

  composite.add_subgroups(subgroups)
  composite.add_view(view)
  #composite.add_view(unstranded_view)

  track_list = []

  for sample in neg_files.keys():
    lab = dcc_data.loc[dcc_data['sequencing_id'] == sample, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == sample, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    pos_track = trackhub.Track(
      name=sample + '_pos',
      short_label=', '.join([rna_stages[sample], lab, sample, tissue]),
      source=pos_files[sample],
      tracktype='bigWig',
      visibility='full',
      autoScale="on",
      maxHeightPixels='128:32:8',
      color = trackhub.helpers.hex2rgb('#FF0000'),
      subgroups={'stage':rna_stages[sample], 'sample':sample, 'strand':'pos'},
    )

    neg_track = trackhub.Track(
      name=sample + '_neg',
      short_label=', '.join([rna_stages[sample], lab, sample, tissue]),
      source=neg_files[sample],
      tracktype='bigWig',
      autoScale="on",
      maxHeightPixels='128:32:8',
      color = trackhub.helpers.hex2rgb('#0000FF'),
      subgroups={'stage':rna_stages[sample], 'sample':sample, 'strand':'neg'},
    )

    track_list.append(pos_track)
    track_list.append(neg_track)

    #overlay = trackhub.AggregateTrack(
    #  aggregate='transparentOverlay',
    #  visibility='full',
    #  tracktype='bigWig',
    #  viewLimits='0:100',
    #  maxHeightPixels='128:32:8',
    #  showSubtrackColorOnUi='on',
    #  name='Stranded' + sample)

    #overlay.add_subtrack(pos_track)
    #overlay.add_subtrack(neg_track)
    #agg_tracks.append(overlay)

  #stranded_view.add_tracks(agg_tracks)
  
  unstranded_tracks = []

  for seq_id, file in unstrand.items():
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    track = trackhub.Track(
        name=seq_id + "_unstranded",
        short_label=', '.join([rna_stages[seq_id], lab, seq_id, tissue]),
        source=file,
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        maxHeightPixels='128:32:8',
        color=trackhub.helpers.hex2rgb('#3182BD'),
        subgroups={'stage':rna_stages[seq_id], 'sample':seq_id, 'strand':'uns'},
      ) 

    track_list.append(track)

  view.add_tracks(track_list)

  #unstranded_view.add_tracks(unstranded_tracks)

  return composite


def CAGEcomposite(assay):

  cage_bw = glob.glob('/'.join([FILES_BASE, assay,'*/*.bigWig']))
  cage_bb = glob.glob('/'.join([FILES_BASE, assay,'*/*.bb']))

  cage_stages = []
  cage_seq_ids = []
  bw_tracks = []

  for bw in cage_bw:
    
    file_name = bw.split('/')[-1]
    seq_id = file_name.split('.')[0]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    cage_stages.append(stage)
    cage_seq_ids.append(seq_id)

    track = trackhub.Track(
        name=seq_id + "_signal",
        short_label=', '.join([stage, lab, seq_id, tissue]),
        source=bw,
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        maxHeightPixels='128:32:8',
        color=trackhub.helpers.hex2rgb("#9ECAE1"),
        subgroups={'stage':stage, 'sample':seq_id, 'kind':'signal'},
      )
    bw_tracks.append(track)

  region_tracks = []

  for track in cage_bb:

    file_name = track.split('/')[-1]
    seq_id = file_name.split('.')[0]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    cage_seq_ids.append(seq_id)

    track_object = trackhub.Track(
      name=seq_id + '_tagCluster',
      short_label=', '.join([stage, lab, seq_id, tissue]),
      source=track,
      tracktype='bigBed',
      visibility='dense',
      itemRgb='on',
      subgroups={'stage':stage, 'sample':seq_id, 'kind':'region'},
    )
      
    region_tracks.append(track_object)
  
  composite = trackhub.CompositeTrack(
    name = 'CAGE-seqComposite',
    short_label='CAGE-seq tracks',
    tracktype='bed 3',
    dimensions="dimX=stage dimA=kind dimB=sample",
    sortOrder="sample=+ kind=- stage=+",
    html_string=dedent(
    """
    CAGE-seq tracks
    ---------------

    This is a CAGE-seq super track that groups together signal (bigWig format) and
    tag cluster regions (bigBed format). These tracks were produced by the DANIO-CODE CAGE-seq pipeline.
    
    The main processing pipeline of the DCC ("CAGE_pipeline_v1.6") consists of 4 different processing steps :
      #.Sequence quality filtering / trimming (FastQC)
      #.Mapping to the reference genome (Bowtie)
      #.Tag clustering (Paraclu-9)

    There is also an additional signal conversion step for reformatting the input from the main pipeline 
    to adequate UCSC track input format (DANIO-CODE CAGE-seq signal conversion pipeline v1.0).
    
    **Track definition**:
        CAGE-seq tracks consist of 2 parts:
        #. CAGE-seq TSS signals
        #. and CAGE-seq tag clusters
    
    For more information on how the data were processed, please refer `here <https://gitlab.com/da-bar/DANIO-CODE_CAGE-seq>`.

    """
      )
    )

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=dict(zip(cage_stages, cage_stages))
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track kind',
        mapping={'signal':'signal','region':'tc'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(cage_seq_ids, cage_seq_ids))
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='regions',
      visibility='dense',
      tracktype='bigBed',
      short_label='Regions')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)  
  regions_view.add_tracks(region_tracks)

  return composite

def ChIPcomposite(assay):

  signals = glob.glob('/zfin/danio-code/non_annotated_files/ChIP-tracks/signals/*.bw')
  peaks = glob.glob('/zfin/danio-code/non_annotated_files/ChIP-tracks/peaks/*.bigNarrowPeak')
  
  chip_stages = []
  targets = []
  signal_tracks = []
  chip_seq_ids = []

  for bw in signals:

    file_name = bw.split('/')[-1]
    seq_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    target = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'target'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    chip_stages.append(stage)
    chip_seq_ids.append(seq_id)
    targets.append(target)
    
    track = trackhub.Track(
        name=target + "_" + seq_id + "_signal",
        short_label=', '.join([target, stage, lab, seq_id, tissue]),
        source=bw,
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        color=trackhub.helpers.hex2rgb("#74C476"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage,'target':target,'kind':'signal', 'sample':seq_id},
      )

    signal_tracks.append(track)

  peak_tracks = []

  for bnp in peaks:

    file_name = bnp.split('/')[-1]
    seq_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    target = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'target'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    chip_seq_ids.append(seq_id)
    
    track = trackhub.Track(
        name=target + "_" + seq_id + "_peak",
        short_label = ', '.join([target, stage, lab, seq_id, tissue]),
        source=bnp,
        visibility='dense',
        tracktype='bigBed 6 4',
        autoScale='on',
        itemRgb='on',
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage,'target':target,'kind':'peak','sample':seq_id},
      )

    peak_tracks.append(track)
  
  composite = trackhub.CompositeTrack(
    name = 'ChIP-seqComposite',
    short_label='ChIP-seq tracks',
    tracktype='bed 3',
    sortOrder='sample=+ kind=- stage=+',
    dimensions="dimX=stage dimY=target dimA=kind dimB=sample",
    html_string=dedent(
    """
    ChIP-seq tracks
    ---------------

    This is a ChIP-seq super track that groups together signal (bigWig format) and narrow peaks (bigNarrowPeak format) of ChIP-seq data.
    regions (bigBed format).

    The ChIP-seq pipeline used to generate these tracks are described `here <https://gitlab.com/danio-code/DANIO-CODE_ChIP-seq>`.

    Briefly, the pipeline consists of:
      #. Aligning reads (bwa): Aligning raw reads to the reference genome.
      #. Filtering of aligned reads: Filtering the ba file for unaligned reads and and duplicates with samtools and sambamba.
      #. Optical duplicates detection: Detect optical duplicates with Picard tools.
      #. Duplicates removal: Second round of filtering with samtools and sambamba.
      #. Cross-correlation analysis.

    **Track definition**:
        ChIP-seq tracks consist of 2 different types:
        #. ChIP-seq signals.
        #. ChIP-seq narrow peaks.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ADANIO-CODE_RNA-seq>`.

    """
      ))

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=dict(zip(chip_stages, chip_stages))
    ),
    trackhub.SubGroupDefinition(
        name='target',
        label='ChIP-seq_target',
        mapping=dict(zip(targets, targets))
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track_kind',
        mapping={'signal':'signal', 'peak':'peak'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(chip_seq_ids, chip_seq_ids))
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')

  peak_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='peaks',
      visibility='dense',
      tracktype='bigBed 6 4',
      short_label='Peaks')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(peak_view)
  
  signal_view.add_tracks(signal_tracks)  
  peak_view.add_tracks(peak_tracks)

  return composite

def MNaseComposite(assay):

  signals = glob.glob('/zfin/danio-code/non_annotated_files/MNase-seq_tracks/*.bigwig')

  mnase_stages = []
  signal_tracks = []
  mnase_bios_ids = []

  for bw in signals:

    file_name = bw.split('/')[-1]
    bios_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'assay_lab_name'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    mnase_stages.append(stage)
    
    track = trackhub.Track(
        name=bios_id + "_signal",
        short_label=', '.join([stage, lab, bios_id, tissue]),
        source=bw,
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        color=trackhub.helpers.hex2rgb("#BAE4B3"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage},
      )

    signal_tracks.append(track)

  composite = trackhub.CompositeTrack(
    name = 'MNase-seqComposite',
    short_label='MNase-seq tracks',
    tracktype='bigWig',
    dimensions="dimX=stage",
    html_string=dedent(
      """
      MNase-seq tracks consist of bigWig files containing negative logarithm of p-value representing the probability of observed signal over expected.
      """
      ))

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=dict(zip(mnase_stages, mnase_stages))
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')
  
  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  
  signal_view.add_tracks(signal_tracks)  

  return composite



# This will change when we will set up a new hub on a different server

FILES_BASE = '/zfin/danio-code/annotated_files'
TRACKHUB_LOC = '/zfin/danio-code/freezer/freeze-02'
GENOME = 'danRer10'
ASSAYS = ["BS-seq", "ATAC-seq", "RNA-seq", "CAGE-seq", "ChIP-seq", "MNase-seq"]
ASSAY_HUB={"BS-seq":BScomposite, "ATAC-seq":ATACcomposite, "RNA-seq": RNAcomposite, "CAGE-seq":CAGEcomposite, "ChIP-seq": ChIPcomposite, "MNase-seq":MNaseComposite}

# Here we will create a new hub object

hub, genomes_file, genome, trackdb = trackhub.default_hub(
	hub_name='DANIO-CODE', # I think this is enough for the name, I suppose
	short_label='DANIO-CODE Track Hub',
	long_label='A Track Hub containing tracks produced with DANIO-CODE pipelines',
	email='daniocode@gmail.com',
	genome=GENOME
	)

#hub.url =  "https://danio-code.zfin.org/trackhub/DANIO-CODE.hub.txt" DEPRACTED
#hub.remote_fn = "/zfin/danio-code/trackhub/DANIO-CODE.hub.txt"

assert trackdb is hub.genomes_file.genomes[0].trackdb

#track = Track(
#   name='testTrack',
#   tracktype='bigWig',
#   short_label='test track',
#   long_label='track for testing purposes',
#   source='ATAC_test_1M_atac.bam_nodup.monoNsome.bw',
#   color='128,0,0'
#	)

stages_dict = dict(zip(dcc_data.stage.unique(), dcc_data.stage.unique()))

for assay in ASSAYS:

  supertrack = trackhub.SuperTrack(
    name = assay,
    short_label = assay + ' tracks',
    )
  
  supertrack.add_tracks(ASSAY_HUB[assay](assay))
  trackdb.add_tracks(supertrack)

#results = hub.render()

kwargs = dict(host = 'localhost', remote_dir = 'test_dir')
#upload_hub(hub=hub, **kwargs)
upload_hub(hub=hub, host='localhost', remote_dir=TRACKHUB_LOC)
#upload_track(track=track, **kwargs)
	
print 'Rendered!'
print 'Finished!'


  



    




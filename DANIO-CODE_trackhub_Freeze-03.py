#!/usr/bin/env python

'''
    A script to create DANIO-CODE Track Hub. It is dependant on 
    DANIO-CODE_tackhub environment.

'''


import trackhub
from trackhub.hub import Hub
from trackhub.genomes_file import GenomesFile
from trackhub.genome import Genome
from trackhub.trackdb import TrackDb
from trackhub.upload import upload_hub, stage_hub
import os
import glob
from textwrap import dedent
import pandas as pd
from collections import OrderedDict

# put here the most updated file
dcc_data = pd.read_csv('DCC-annotations_datafreeze03_corrected.csv', delimiter = ";", na_values = "no value")
all_stages = ['1-cell',
              '2-cell',
              '4-cell',
              '16-cell',
              '32-cell',
              '64-cell',
              '128-cell',
              '256-cell',
              '512-cell',
              '1k-cell',
              'High',
              'Oblong',
              'Sphere',
              'Dome',
              '30%-epiboly',
              '50%-epiboly',
              'Germ-ring',
              'Shield',
              '75%-epiboly',
              '90%-epiboly',
              'Bud',
              '1-4 somites',
              '5-9 somites',
              '10-13 somites',
              '14-19 somites',
              '20-25 somites',
              '26+ somites',
              'Prim-5',
              'Prim-15',
              'Prim-25',
              'High-pec',
              'Long-pec',
              'Pec-fin',
              'Protruding-mouth',
              'Day 4',
              'Day 5',
              'Day 6',
              'Days 7-13',
              '90 Days-2 Years',
              'nan']
all_stages = [str(s).replace(' ','_') for s in all_stages]
all_stages_enumerated = enumerate(all_stages)
all_stages_enumerated = {j:"_".join([str(i).zfill(2),j]) for i,j in all_stages_enumerated}

# The assay functions

# Don't touch BS-seq for now

def BScomposite(assay):

  bs_bw = glob.glob('/'.join([FILES_BASE, assay, '*/*.bw']))
  bs_cpg = glob.glob('/'.join([FILES_BASE, assay, '*/*CpG.bb']))
  bs_chg = glob.glob('/'.join([FILES_BASE, assay, '*/*CHG.bb']))
  bs_chh = glob.glob('/'.join([FILES_BASE, assay, '*/*CHH.bb']))
  bs_stages = []
  bs_seq_ids = []

  bw_tracks = []

  for bw in bs_bw:

    seq_id = bw.split('.')[1]
    stage = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0].replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo =  str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')
    bs_stages.append(stage.replace(' ','_'))
    bs_seq_ids.append(seq_id)

    track = trackhub.Track(
        name=seq_id + "_signal",
        source=bw,
        short_label=', '.join([assay, stage, lab, seq_id, tissue, geo]),
        visibility='hide',
        tracktype='bigWig',
        viewLimits='0:100',
        color=trackhub.helpers.hex2rgb("#DE2D26"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage, 'context':'all', 'sample':seq_id,'kind':'signal', 'enstage': all_stages_enumerated[stage]},
      )
    bw_tracks.append(track)

  composite = trackhub.CompositeTrack(
    name = 'BSseqComposite',
    short_label='BS-seq tracks',
    tracktype='bed 3',
    sortOrder='enstage=+ sample=+',
    dimensions="dimX=stage dimY=context dimA=kind dimB=sample dimC=enstage",
    filterComposite="dimA dimB dimC"
    )
  
  subgroup_stages = [stage for stage in all_stages if stage in bs_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  context_mapping = OrderedDict([
            ('CpG', 'CpG'),
            ('CHG', 'CHG'),
            ('CHH', 'CHH'),
            ('all', 'all'),
        ]) 
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in bs_stages}   
  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track
    ),
    trackhub.SubGroupDefinition(
        name='context',
        label='Context',
        mapping=context_mapping,          
     ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Kind',
        mapping={'signal':'signal','region':'region'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sample',
      mapping=dict(zip(bs_seq_ids, bs_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      color=trackhub.helpers.hex2rgb("#DE2D26"),
      short_label='Methylation signal')

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='region',
      visibility='dense',
      tracktype='bigBed 9 +',
      short_label='Methylated Cs')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)

  def region_tracks(context, tracks):

    context_tracks = []

    for track in tracks:

      seq_id = track.split('.')[1]
      stage = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0].replace(' ', '_')
      lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
      tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
      geo =  str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')


      track_object = trackhub.Track(
        name=context + "_" + seq_id,
        source=track,
        long_label=', '.join([context, stage, lab, tissue, geo]),
        tracktype='bigBed 9 +',
        visibility='dense',
        itemRgb='on',
        subgroups={'stage':stage, 'context': context, 'kind':'region', 'sample':seq_id, 'enstage': all_stages_enumerated[stage]},
      )
      
      context_tracks.append(track_object)

    return(context_tracks)

  regions_view.add_tracks(region_tracks('CpG', bs_cpg))
  regions_view.add_tracks(region_tracks('CHG', bs_chg))
  regions_view.add_tracks(region_tracks('CHH', bs_chh))

  return composite


# Don't touch ATAC-seq as well for now

def ATACcomposite(assay):

  atac_bw = dcc_data[(dcc_data.assay_type == "ATAC-seq") & (dcc_data.loc[:, "primary_file.name"].str.contains("pval.signal.bigwig")) & (dcc_data.mapped_genome == "GRCz10/danRer10")].loc[:,"primary_file.name"].tolist()
  #atac_bw = ["/".join([FILES_BASE, i]) for i in atac_bw]
  # have to import the files!!!
  #atac_bnw = glob.glob('/zfin/danio-code/non_annotated_files/atac_seq_results/bigNarrowPeaks/*.bigNarrowPeak')
  atac_bnw = dcc_data[(dcc_data.assay_type == "ATAC-seq") & (dcc_data.loc[:, "primary_file.name"].str.contains("bigNarrowPeak")) & (dcc_data.mapped_genome == "GRCz10/danRer10")].loc[:,"primary_file.name"].tolist()
  #atac_bnw = ["/".join([FILES_BASE, i]) for i in atac_bnw]
  atac_stages = []
  atac_seq_ids = []
  bw_tracks = []

  for bw in atac_bw:

    seq_id = bw.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    #print seq_id
    #print stage
    data_id = str(dcc_data[dcc_data.loc[:,"primary_file.name"] == bw].data_id.tolist()[0])
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    atac_stages.append(stage)
    atac_seq_ids.append(seq_id)

    if stage == 'Prim-5':
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name="_".join([seq_id,data_id,"signal"]),
        long_label=', '.join([stage, lab, tissue, geo]),
        source="/".join([FILES_BASE,bw]),
        visibility=visibility,
        tracktype='bigWig',
        autoScale='on',
        color=trackhub.helpers.hex2rgb("#238B45"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage, 'kind':'signal', 'sample':seq_id, 'enstage': all_stages_enumerated[stage]},
      )
    bw_tracks.append(track)

  region_tracks = []

  for track in atac_bnw:

    seq_id = track.split('.')[1]
    data_id = str(dcc_data[dcc_data.loc[:,"primary_file.name"] == track].data_id.tolist()[0])
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    if stage == 'Prim-5':
      visibility = 'dense'
    else:
      visibility = 'hide'


    atac_seq_ids.append(seq_id)

    track_object = trackhub.Track(
      name="_".join([seq_id, data_id, 'region']),
      long_label = ', '.join([stage, lab, tissue, geo]),
      source="/".join([FILES_BASE, track]),
      tracktype='bigBed 6 4',
      visibility=visibility,
      itemRgb='on',
      subgroups={'stage':stage, 'kind':'region', 'sample':seq_id, 'enstage':all_stages_enumerated[stage]},
    )
      
    region_tracks.append(track_object)
  
  composite = trackhub.CompositeTrack(
    name = 'ATAC-seqComposite',
    short_label='ATAC-seq tracks',
    tracktype='bed 3',
    sortOrder="enstage=+ sample=+ kind=-",
    dimensions="dimX=stage dimA=kind dimB=sample dimC=enstage",
    filterComposite="dimA dimB",
    html_string=dedent(
    """
    ATAC-seq tracks
    ---------------

    This is an ATAC-seq super track that groups together signal of ATAC-seq data (bigWig format) together with the narrow peak signal (bigNarrowPeak format).
    regions (bigBed format).

    The ATAC-seq pipeline used in the analysis is an adapted 
    ATAC-Seq / DNase-Seq Pipeline developed at Kundaje lab 
    (https://github.com/kundajelab/atac_dnase_pipelines). The pipeline consists of 
    the following steps:


    - Quality filtering and trimming of raw reads (Trim-galore)
    - Mapping reads to the reference genome (Bowtie2)
    - Filtering of reads in the output bam file (samtools and sambamba)
    - Marking optical duplicates (Picard tools)
    - Deduplication (samtools and sambamba)
    - Format conversion to a more manageable tagAlign format (https://genome.ucsc.edu/FAQ/FAQformat.html#format15)
    - Tn5 shifting of aligned reads
    - Peak calling (Macs2)

    The analysis also include fragment length distribution of aligned read pairs,
    which allows separation of reads into nucleosome-free region spanning, 
    mononucleosome spanning, and di and more nucleosome spanning.

    **Track definition**:
        ATAC-seq tracks consist of 2 parts:
        #. ATAC-seq signal.
        #. Narrow peak signals.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ATAC-seq>`.

    """
      )
    )
  
  subgroup_stages = [stage for stage in all_stages if stage in atac_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in atac_stages}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track kind',
        mapping={'signal':'signal','region':'region'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(atac_seq_ids, atac_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerate_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      tracktype='bigWig',
      short_label='ATAC-seq Signals',
      color=trackhub.helpers.hex2rgb("#238B45"))

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='regions',
      tracktype='bigBed 6 4',
      short_label='ATAC-seq peaks')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)
  regions_view.add_tracks(region_tracks)

  return composite

# Modify RNA-seq

def RNAcomposite(assay):

  #neg = glob.glob('/'.join([FILES_BASE, assay, "*/*.neg.bw"]))
  #pos = glob.glob('/'.join([FILES_BASE, assay,"*/*.pos.bw"]))

  # better solution extract everything from the pandas df
  # heavy optimisation needed. All the data should be extracted from the df not from file names
  # pos should be distinguishted from unstranded by presence of sequencing_id in the neg df

  rna_bigwigs = dcc_data[(dcc_data.file_type == "BIGWIG") & (dcc_data.assay_type == "RNA-seq") & (dcc_data.mapped_genome == "GRCz10/danRer10")].loc[:,['primary_file.name', 'sequencing_id', 'stage']]
  neg = rna_bigwigs[rna_bigwigs.loc[:, 'primary_file.name'].str.contains('.neg.bw')]
  pos = rna_bigwigs[rna_bigwigs.loc[:, 'primary_file.name'].str.contains('.pos.bw')]

  neg_files = neg_files = dict(zip(neg.sequencing_id, 
    ["/".join([FILES_BASE,fl]) for fl in neg.loc[:, 'primary_file.name']]))

  #for f in neg:
  #  seq_id = f.split('.')[1]
  #  neg_files[seq_id] = f
  
  unstrand = {}
  pos_files = {}

  stranded_seqids = neg_files.keys()
  rna_stages = {}

  #seq_id = pos.sequencing_id.tolist()
  pos = ["/".join([FILES_BASE, fl]) for fl in pos.loc[:, 'primary_file.name'].tolist()]

  for f in pos:
    seq_id = f.split('.')[1]
    stage = str(dcc_data[dcc_data['sequencing_id'] == seq_id].loc[:,'stage'].tolist()[0]).replace(' ', '_')
    rna_stages[seq_id] = stage
    if seq_id in stranded_seqids:
        pos_files[seq_id] = f
    else:
        unstrand[seq_id] = f 

  composite = trackhub.CompositeTrack(
    name = 'RNA-seqComposite',
    short_label='RNA-seq tracks',
    tracktype='bigWig',
    sortOrder="enstage=+ sample=+ strand=-",
    dimensions="dimX=stage dimA=strand dimB=sample dimC=enstage",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    RNA-seq tracks
    ---------------

    This is a RNA-seq super track that groups together signal of RNA-seq data (bigWig format) of stranded and unstranded RNA-seq samples.
    regions (bigBed format).

    The RNA-seq pipeline used to generate these tracks are described `here <https://gitlab.com/danio-code/DANIO-CODE_RNA-seq>`.

    Briefly, the pipeline consists of:
      #. Reference genome index generation (one time only).
      #. Alignment files with STAR: The part of the pipeline which maps the reads to the reference genome.
      #. Quality control of alignment: The second step which controls the quality of the alignments from STAR.
      #. bedGraph to bigWig signal conversion.

    **Track definition**:
        RNA-seq tracks consist of 2 different types:
        #. RNA-seq signals for stranded samples, which consist of positive and negative strands.
        #. RNA-seq signals for stranded samples, which consist of collapsed positive strands.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ADANIO-CODE_RNA-seq>`.

    """
      ))

  subgroup_stages = [stage for stage in all_stages if stage in rna_stages.values()]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in rna_stages.values()}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
      name='strand',
      label='Strand',
      mapping={'pos':'+', 'neg':'-', 'uns':'unstranded'}
      ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(stranded_seqids + unstrand.keys(), stranded_seqids + unstrand.keys()))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  view = trackhub.ViewTrack(
      name='Track_view',
      view='signal',
      short_label='RNA-seq signal')

  #unstranded_view = trackhub.ViewTrack(
  #    name='signalviewtrack',
  #    view='usignal',
  #    visibility='full',
  #    tracktype = 'bigWig',
  #    short_label='Unstranded Signal')

  composite.add_subgroups(subgroups)
  composite.add_view(view)
  #composite.add_view(unstranded_view)

  track_list = []

  for sample in neg_files.keys():
    lab = dcc_data.loc[dcc_data['sequencing_id'] == sample, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == sample, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == sample, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    if rna_stages[sample] == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    pos_track = trackhub.Track(
      name=sample + '_pos',
      long_label=', '.join([rna_stages[sample], lab, tissue, geo]),
      source=pos_files[sample],
      tracktype='bigWig',
      visibility=visibility,
      autoScale="on",
      maxHeightPixels='128:32:8',
      color = trackhub.helpers.hex2rgb('#FF0000'),
      subgroups={'stage':rna_stages[sample], 'sample':sample, 'strand':'pos', 'enstage': all_stages_enumerated[rna_stages[sample]]},
    )

    neg_track = trackhub.Track(
      name=sample + '_neg',
      long_label=', '.join([rna_stages[sample], lab, tissue, geo]),
      source=neg_files[sample],
      tracktype='bigWig',
      visibility = visibility,
      autoScale="on",
      maxHeightPixels='128:32:8',
      color = trackhub.helpers.hex2rgb('#0000FF'),
      subgroups={'stage':rna_stages[sample], 'sample':sample, 'strand':'neg', 'enstage': all_stages_enumerated[rna_stages[sample]]},
    )

    track_list.append(pos_track)
    track_list.append(neg_track)

    #overlay = trackhub.AggregateTrack(
    #  aggregate='transparentOverlay',
    #  visibility='full',
    #  tracktype='bigWig',
    #  viewLimits='0:100',
    #  maxHeightPixels='128:32:8',
    #  showSubtrackColorOnUi='on',
    #  name='Stranded' + sample)

    #overlay.add_subtrack(pos_track)
    #overlay.add_subtrack(neg_track)
    #agg_tracks.append(overlay)

  #stranded_view.add_tracks(agg_tracks)
  
  unstranded_tracks = []

  for seq_id, file in unstrand.items():
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    if rna_stages[sample] == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name=seq_id + "_unstranded",
        long_label=', '.join([rna_stages[seq_id], lab, tissue, geo]),
        source=file,
        visibility=visibility,
        tracktype='bigWig',
        autoScale='on',
        maxHeightPixels='128:32:8',
        color=trackhub.helpers.hex2rgb('#7a0177'),
        subgroups={'stage':rna_stages[seq_id], 'sample':seq_id, 'strand':'uns', 'enstage': all_stages_enumerated[rna_stages[seq_id]]},
      ) 

    track_list.append(track)

  view.add_tracks(track_list)

  #unstranded_view.add_tracks(unstranded_tracks)

  return composite

def RNAcomposite_dr11(assay):

  #neg = glob.glob('/'.join([FILES_BASE, assay, "*/*.neg.bw"]))
  #pos = glob.glob('/'.join([FILES_BASE, assay,"*/*.pos.bw"]))

  # better solution extract everything from the pandas df
  # heavy optimisation needed. All the data should be extracted from the df not from file names
  # pos should be distinguishted from unstranded by presence of sequencing_id in the neg df
  # should combine both danRer10 and danRer11

  rna_bigwigs = dcc_data[(dcc_data.file_type == "BIGWIG") & (dcc_data.assay_type == "RNA-seq") & (dcc_data.mapped_genome == "GRCz11/danRer11")].loc[:,['primary_file.name', 'sequencing_id', 'stage']]
  neg = rna_bigwigs[rna_bigwigs.loc[:, 'primary_file.name'].str.contains('.neg.bw')]
  pos = rna_bigwigs[rna_bigwigs.loc[:, 'primary_file.name'].str.contains('.pos.bw')]

  neg_files = neg_files = dict(zip(neg.sequencing_id, 
    ["/".join([FILES_BASE,fl]) for fl in neg.loc[:, 'primary_file.name']]))

  #for f in neg:
  #  seq_id = f.split('.')[1]
  #  neg_files[seq_id] = f
  
  unstrand = {}
  pos_files = {}

  stranded_seqids = neg_files.keys()
  rna_stages = {}

  #seq_id = pos.sequencing_id.tolist()
  pos = ["/".join([FILES_BASE, fl]) for fl in pos.loc[:, 'primary_file.name'].tolist()]

  for f in pos:
    seq_id = f.split('.')[1]
    stage = str( dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    rna_stages[seq_id] = stage
    if seq_id in stranded_seqids:
        pos_files[seq_id] = f
    else:
        unstrand[seq_id] = f 

  composite = trackhub.CompositeTrack(
    name = 'RNA-seqComposite',
    short_label='RNA-seq tracks',
    tracktype='bigWig',
    sortOrder="enstage=+ sample=+ strand=-",
    dimensions="dimX=stage dimA=strand dimB=sample dimC=enstage",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    RNA-seq tracks
    ---------------

    This is a RNA-seq super track that groups together signal of RNA-seq data (bigWig format) of stranded and unstranded RNA-seq samples.
    regions (bigBed format).

    The RNA-seq pipeline used to generate these tracks are described `here <https://gitlab.com/danio-code/DANIO-CODE_RNA-seq>`.

    Briefly, the pipeline consists of:
      #. Reference genome index generation (one time only).
      #. Alignment files with STAR: The part of the pipeline which maps the reads to the reference genome.
      #. Quality control of alignment: The second step which controls the quality of the alignments from STAR.
      #. bedGraph to bigWig signal conversion.

    **Track definition**:
        RNA-seq tracks consist of 2 different types:
        #. RNA-seq signals for stranded samples, which consist of positive and negative strands.
        #. RNA-seq signals for stranded samples, which consist of collapsed positive strands.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ADANIO-CODE_RNA-seq>`.

    """
      ))

  subgroup_stages = [stage for stage in all_stages if stage in rna_stages.values()]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in rna_stages.values()}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
      name='strand',
      label='Strand',
      mapping={'pos':'+', 'neg':'-', 'uns':'unstranded'}
      ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(stranded_seqids + unstrand.keys(), stranded_seqids + unstrand.keys()))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  view = trackhub.ViewTrack(
      name='Track_view',
      view='signal',
      short_label='RNA-seq signal')

  #unstranded_view = trackhub.ViewTrack(
  #    name='signalviewtrack',
  #    view='usignal',
  #    visibility='full',
  #    tracktype = 'bigWig',
  #    short_label='Unstranded Signal')

  composite.add_subgroups(subgroups)
  composite.add_view(view)
  #composite.add_view(unstranded_view)

  track_list = []

  # need to extract the lab info and everything else from df
  
  for sample in neg_files.keys():
    lab = dcc_data.loc[dcc_data['sequencing_id'] == sample, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == sample, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == sample, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    if rna_stages[sample] == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    pos_track = trackhub.Track(
      name=sample + '_pos',
      long_label=', '.join([rna_stages[sample], lab, tissue, geo]),
      source=pos_files[sample],
      tracktype='bigWig',
      visibility=visibility,
      autoScale="on",
      maxHeightPixels='128:32:8',
      color = trackhub.helpers.hex2rgb('#FF0000'),
      subgroups={'stage':rna_stages[sample], 'sample':sample, 'strand':'pos', 'enstage': all_stages_enumerated[rna_stages[sample]]},
    )

    neg_track = trackhub.Track(
      name=sample + '_neg',
      long_label=', '.join([rna_stages[sample], lab, tissue, geo]),
      source=neg_files[sample],
      tracktype='bigWig',
      visibility = visibility,
      autoScale="on",
      maxHeightPixels='128:32:8',
      color = trackhub.helpers.hex2rgb('#0000FF'),
      subgroups={'stage':rna_stages[sample], 'sample':sample, 'strand':'neg', 'enstage': all_stages_enumerated[rna_stages[sample]]},
    )

    track_list.append(pos_track)
    track_list.append(neg_track)

    #overlay = trackhub.AggregateTrack(
    #  aggregate='transparentOverlay',
    #  visibility='full',
    #  tracktype='bigWig',
    #  viewLimits='0:100',
    #  maxHeightPixels='128:32:8',
    #  showSubtrackColorOnUi='on',
    #  name='Stranded' + sample)

    #overlay.add_subtrack(pos_track)
    #overlay.add_subtrack(neg_track)
    #agg_tracks.append(overlay)

  #stranded_view.add_tracks(agg_tracks)
  
  unstranded_tracks = []

  for seq_id, file in unstrand.items():
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    if rna_stages[sample] == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name=seq_id + "_unstranded",
        long_label=', '.join([rna_stages[seq_id], lab, tissue, geo]),
        source=file,
        visibility=visibility,
        tracktype='bigWig',
        autoScale='on',
        maxHeightPixels='128:32:8',
        color=trackhub.helpers.hex2rgb('#7a0177'),
        subgroups={'stage':rna_stages[seq_id], 'sample':seq_id, 'strand':'uns', 'enstage': all_stages_enumerated[rna_stages[seq_id]]},
      ) 

    track_list.append(track)

  view.add_tracks(track_list)

  #unstranded_view.add_tracks(unstranded_tracks)

  return composite

def CAGEcomposite(assay):

  cage_bw = dcc_data[(dcc_data.assay_type == "CAGE-seq") & (dcc_data.file_type == "BIGWIG") & (dcc_data.mapped_genome == "GRCz10/danRer10")].loc[:,"primary_file.name"].tolist()
  #cage_bw = ['/'.join([FILES_BASE, i]) for i in cage_bw]
  #cage_bb = glob.glob('/'.join([FILES_BASE, assay,'*/*.bb']))
  cage_bb = dcc_data[(dcc_data.assay_type == "CAGE-seq") & (dcc_data.file_type == "BIGBED") & (dcc_data.mapped_genome == "GRCz10/danRer10")].loc[:,"primary_file.name"].tolist()
  #cage_bb = ['/'.join([FILES_BASE, i]) for i in cage_bb]

  cage_stages = []
  cage_seq_ids = []
  bw_tracks = []

  for bw in cage_bw:
    
    file_name = bw.split('/')[-1]
    seq_id = file_name.split('.')[1]
    #print seq_id
    data_id = str(dcc_data[dcc_data.loc[:,"primary_file.name"] == bw].data_id.tolist()[0])
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    cage_stages.append(stage)
    cage_seq_ids.append(seq_id)

    if stage == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name="_".join([seq_id, data_id, "signal"]),
        long_label=', '.join([stage, lab, tissue, geo]),
        source="/".join([FILES_BASE,bw]),
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        maxHeightPixels='128:32:8',
        color=trackhub.helpers.hex2rgb("#9ECAE1"),
        subgroups={'stage':stage, 'sample':seq_id, 'kind':'signal', 'enstage':all_stages_enumerated[stage]},
      )
    bw_tracks.append(track)

  region_tracks = []

  for track in cage_bb:

    file_name = track.split('/')[-1]
    seq_id = file_name.split('.')[1]
    data_id = str(dcc_data[dcc_data.loc[:,"primary_file.name"] == track].data_id.tolist()[0])
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    cage_seq_ids.append(seq_id)

    if stage == "Prim-5":
      visibility = 'dense'
    else:
      visibility = 'hide'

    track_object = trackhub.Track(
      name="_".join([seq_id, data_id, 'tagCluster']),
      long_label=', '.join([stage, lab, tissue, geo]),
      source="/".join([FILES_BASE, track]),
      tracktype='bigBed',
      visibility='dense',
      itemRgb='on',
      subgroups={'stage':stage, 'sample':seq_id, 'kind':'region', 'enstage':all_stages_enumerated[stage]},
    )
      
    region_tracks.append(track_object)
  
  composite = trackhub.CompositeTrack(
    name = 'CAGE-seqComposite',
    short_label='CAGE-seq tracks',
    tracktype='bed 3',
    dimensions="dimX=stage dimA=kind dimB=sample dimC=enstage",
    sortOrder="enstage=+ sample=+ kind=-",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    CAGE-seq tracks
    ---------------

    This is a CAGE-seq super track that groups together signal (bigWig format) and
    tag cluster regions (bigBed format). These tracks were produced by the DANIO-CODE CAGE-seq pipeline.
    
    The main processing pipeline of the DCC ("CAGE_pipeline_v1.6") consists of 4 different processing steps :
      #.Sequence quality filtering / trimming (FastQC)
      #.Mapping to the reference genome (Bowtie)
      #.Tag clustering (Paraclu-9)

    There is also an additional signal conversion step for reformatting the input from the main pipeline 
    to adequate UCSC track input format (DANIO-CODE CAGE-seq signal conversion pipeline v1.0).
    
    **Track definition**:
        CAGE-seq tracks consist of 2 parts:
        #. CAGE-seq TSS signals
        #. and CAGE-seq tag clusters
    
    For more information on how the data were processed, please refer `here <https://gitlab.com/da-bar/DANIO-CODE_CAGE-seq>`.

    """
      )
    )

  subgroup_stages = [stage for stage in all_stages if stage in cage_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in cage_stages}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track kind',
        mapping={'signal':'signal','region':'tc'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(cage_seq_ids, cage_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='regions',
      visibility='dense',
      tracktype='bigBed',
      short_label='Regions')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)  
  regions_view.add_tracks(region_tracks)

  return composite

def CAGEcomposite_dr11(assay):

  cage_bw = dcc_data[(dcc_data.assay_type == "CAGE-seq") & (dcc_data.loc[:,"primary_file.name"].str.contains("duplicatesremoved.bigWig")) & (dcc_data.mapped_genome == "GRCz11/danRer11")].loc[:,"primary_file.name"].tolist()
  #cage_bw = ['/'.join([FILES_BASE, i]) for i in cage_bw]
  #cage_bb = glob.glob('/'.join([FILES_BASE, assay,'*/*.bb']))
  cage_bb = dcc_data[(dcc_data.assay_type == "CAGE-seq") & (dcc_data.loc[:,"primary_file.name"].str.contains("sorted.bb")) & (dcc_data.mapped_genome == "GRCz11/danRer11")].loc[:,"primary_file.name"].tolist()
  #cage_bb = ['/'.join([FILES_BASE, i]) for i in cage_bb]

  cage_stages = []
  cage_seq_ids = []
  bw_tracks = []

  for bw in cage_bw:
    
    file_name = bw.split('/')[-1]
    seq_id = file_name.split('.')[1]
    data_id = str(dcc_data[dcc_data.loc[:,"primary_file.name"] == bw].data_id.tolist()[0])
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    cage_stages.append(stage)
    cage_seq_ids.append(seq_id)

    if stage == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name="_".join([seq_id, data_id, "signal"]),
        long_label=', '.join([stage, lab, tissue, geo]),
        source="/".join([FILES_BASE,bw]),
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        maxHeightPixels='128:32:8',
        color=trackhub.helpers.hex2rgb("#9ECAE1"),
        subgroups={'stage':stage, 'sample':seq_id, 'kind':'signal', 'enstage':all_stages_enumerated[stage]},
      )
    bw_tracks.append(track)

  region_tracks = []

  for track in cage_bb:

    file_name = track.split('/')[-1]
    seq_id = file_name.split('.')[1]
    data_id = str(dcc_data[dcc_data.loc[:,"primary_file.name"] == track].data_id.tolist()[0])
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    cage_seq_ids.append(seq_id)

    if stage == "Prim-5":
      visibility = 'dense'
    else:
      visibility = 'hide'

    track_object = trackhub.Track(
      name="_".join([seq_id, data_id, 'tagCluster']),
      long_label=', '.join([stage, lab, tissue, geo]),
      source="/".join([FILES_BASE,track]),
      tracktype='bigBed',
      visibility='dense',
      itemRgb='on',
      subgroups={'stage':stage, 'sample':seq_id, 'kind':'region', 'enstage':all_stages_enumerated[stage]},
    )
      
    region_tracks.append(track_object)
  
  composite = trackhub.CompositeTrack(
    name = 'CAGE-seqComposite',
    short_label='CAGE-seq tracks',
    tracktype='bed 3',
    dimensions="dimX=stage dimA=kind dimB=sample dimC=enstage",
    sortOrder="enstage=+ sample=+ kind=-",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    CAGE-seq tracks
    ---------------

    This is a CAGE-seq super track that groups together signal (bigWig format) and
    tag cluster regions (bigBed format). These tracks were produced by the DANIO-CODE CAGE-seq pipeline.
    
    The main processing pipeline of the DCC ("CAGE_pipeline_v1.6") consists of 4 different processing steps :
      #.Sequence quality filtering / trimming (FastQC)
      #.Mapping to the reference genome (Bowtie)
      #.Tag clustering (Paraclu-9)

    There is also an additional signal conversion step for reformatting the input from the main pipeline 
    to adequate UCSC track input format (DANIO-CODE CAGE-seq signal conversion pipeline v1.0).
    
    **Track definition**:
        CAGE-seq tracks consist of 2 parts:
        #. CAGE-seq TSS signals
        #. and CAGE-seq tag clusters
    
    For more information on how the data were processed, please refer `here <https://gitlab.com/da-bar/DANIO-CODE_CAGE-seq>`.

    """
      )
    )

  subgroup_stages = [stage for stage in all_stages if stage in cage_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in cage_stages}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track kind',
        mapping={'signal':'signal','region':'tc'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(cage_seq_ids, cage_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='regions',
      visibility='dense',
      tracktype='bigBed',
      short_label='Regions')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)  
  regions_view.add_tracks(region_tracks)

  return composite

def ThreeP_composite(assay):

  cage_bw = dcc_data[(dcc_data.assay_type == "3P-seq") & (dcc_data.file_type == "BIGWIG") & (dcc_data.mapped_genome == "GRCz10/danRer10")].loc[:,"primary_file.name"].tolist()
  cage_bw = ['/'.join([FILES_BASE, i]) for i in cage_bw]
  #cage_bb = glob.glob('/'.join([FILES_BASE, assay,'*/*.bb']))
  cage_bb = dcc_data[(dcc_data.assay_type == "3P-seq") & (dcc_data.file_type == "BIGBED") & (dcc_data.mapped_genome == "GRCz10/danRer10")].loc[:,"primary_file.name"].tolist()
  cage_bb = ['/'.join([FILES_BASE, i]) for i in cage_bb]

  cage_stages = []
  cage_seq_ids = []
  bw_tracks = []

  for bw in cage_bw:
    
    file_name = bw.split('/')[-1]
    seq_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    cage_stages.append(stage)
    cage_seq_ids.append(seq_id)

    if stage == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name=seq_id + "_signal",
        long_label=', '.join([stage, lab, tissue, geo]),
        source=bw,
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        maxHeightPixels='128:32:8',
        color=trackhub.helpers.hex2rgb("#9ECAE1"),
        subgroups={'stage':stage, 'sample':seq_id, 'kind':'signal', 'enstage':all_stages_enumerated[stage]},
      )
    bw_tracks.append(track)

  region_tracks = []

  for track in cage_bb:

    file_name = track.split('/')[-1]
    seq_id = file_name.split('.')[1]
    seq_id = seq_id.split('_')[0]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    cage_seq_ids.append(seq_id)

    if stage == "Prim-5":
      visibility = 'dense'
    else:
      visibility = 'hide'

    track_object = trackhub.Track(
      name=seq_id + '_tagCluster',
      long_label=', '.join([stage, lab, tissue, geo]),
      source=track,
      tracktype='bigBed',
      visibility='dense',
      itemRgb='on',
      subgroups={'stage':stage, 'sample':seq_id, 'kind':'region', 'enstage':all_stages_enumerated[stage]},
    )
      
    region_tracks.append(track_object)
  
  composite = trackhub.CompositeTrack(
    name = '3P-seqComposite',
    short_label='3P-seq tracks',
    tracktype='bed 3',
    dimensions="dimX=stage dimA=kind dimB=sample dimC=enstage",
    sortOrder="enstage=+ sample=+ kind=-",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    CAGE-seq tracks
    ---------------

    This is a CAGE-seq super track that groups together signal (bigWig format) and
    tag cluster regions (bigBed format). These tracks were produced by the DANIO-CODE CAGE-seq pipeline.
    
    The main processing pipeline of the DCC ("CAGE_pipeline_v1.6") consists of 4 different processing steps :
      #.Sequence quality filtering / trimming (FastQC)
      #.Mapping to the reference genome (Bowtie)
      #.Tag clustering (Paraclu-9)

    There is also an additional signal conversion step for reformatting the input from the main pipeline 
    to adequate UCSC track input format (DANIO-CODE CAGE-seq signal conversion pipeline v1.0).
    
    **Track definition**:
        CAGE-seq tracks consist of 2 parts:
        #. CAGE-seq TSS signals
        #. and CAGE-seq tag clusters
    
    For more information on how the data were processed, please refer `here <https://gitlab.com/da-bar/DANIO-CODE_CAGE-seq>`.

    """
      )
    )

  subgroup_stages = [stage for stage in all_stages if stage in cage_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in cage_stages}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track kind',
        mapping={'signal':'signal','region':'tc'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(cage_seq_ids, cage_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='regions',
      visibility='dense',
      tracktype='bigBed',
      short_label='Regions')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)  
  regions_view.add_tracks(region_tracks)

  return composite

def ThreeP_composite_dr11(assay):

  cage_bw = dcc_data[(dcc_data.assay_type == "3P-seq") & (dcc_data.file_type == "BIGWIG") & (dcc_data.mapped_genome == "GRCz11/danRer11")].loc[:,"primary_file.name"].tolist()
  cage_bw = ['/'.join([FILES_BASE, i]) for i in cage_bw]
  #cage_bb = glob.glob('/'.join([FILES_BASE, assay,'*/*.bb']))
  cage_bb = dcc_data[(dcc_data.assay_type == "3P-seq") & (dcc_data.file_type == "BIGBED") & (dcc_data.mapped_genome == "GRCz11/danRer11")].loc[:,"primary_file.name"].tolist()
  cage_bb = ['/'.join([FILES_BASE, i]) for i in cage_bb]

  cage_stages = []
  cage_seq_ids = []
  bw_tracks = []

  for bw in cage_bw:
    
    file_name = bw.split('/')[-1]
    seq_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['data_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['data_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['data_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['data_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    cage_stages.append(stage)
    cage_seq_ids.append(seq_id)

    if stage == "Prim-5":
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name=seq_id + "_signal",
        long_label=', '.join([stage, lab, tissue, geo]),
        source=bw,
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        maxHeightPixels='128:32:8',
        color=trackhub.helpers.hex2rgb("#9ECAE1"),
        subgroups={'stage':stage, 'sample':seq_id, 'kind':'signal', 'enstage':all_stages_enumerated[stage]},
      )
    bw_tracks.append(track)

  region_tracks = []

  for track in cage_bb:

    file_name = track.split('/')[-1]
    seq_id = file_name.split('.')[1]
    seq_id = seq_id.split('_')[0]
    stage = str(dcc_data.loc[dcc_data['data_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['data_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['data_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    cage_seq_ids.append(seq_id)

    if stage == "Prim-5":
      visibility = 'dense'
    else:
      visibility = 'hide'

    track_object = trackhub.Track(
      name=seq_id + '_tagCluster',
      long_label=', '.join([stage, lab, tissue, geo]),
      source=track,
      tracktype='bigBed',
      visibility='dense',
      itemRgb='on',
      subgroups={'stage':stage, 'sample':seq_id, 'kind':'region', 'enstage':all_stages_enumerated[stage]},
    )
      
    region_tracks.append(track_object)
  
  composite = trackhub.CompositeTrack(
    name = '3P-seqComposite',
    short_label='3P-seq tracks',
    tracktype='bed 3',
    dimensions="dimX=stage dimA=kind dimB=sample dimC=enstage",
    sortOrder="enstage=+ sample=+ kind=-",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    3P-seq tracks
    ---------------
    This is a 3P-seq super track that groups together signal (bigWig format) and
    tag clusters of transription end site (TES) (bigBed format). These tracks were produced by the DANIO-CODE 3P-seq pipeline.
    
    The DANIO-CODE 3P-seq pipeline consists of 5 steps:
        #.Step 1. Filter raw data 
        #.Step 2. Quality Control (FastQC)
        #.Step 3. Mapping to reference genome (Bowtie)
        #.Step 4. Obtaining Transcription End Sites (TES) from 3P-seq data
        #.Step 5. Generating Tag Clusters of TES  (Paraclu-9)
        
    
    **Track definition**:
        3P-seq tracks consist of 2 parts:
        #. 3P-seq TES signals
        #. and 3P-seq tag clusters of TES
    
    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/DANIO-CODE_3P-seq>`.
    
    """
      )
    )

  subgroup_stages = [stage for stage in all_stages if stage in cage_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in cage_stages}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track kind',
        mapping={'signal':'signal','region':'tc'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(cage_seq_ids, cage_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')

  regions_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='regions',
      visibility='dense',
      tracktype='bigBed',
      short_label='Regions')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(regions_view)
  
  signal_view.add_tracks(bw_tracks)  
  regions_view.add_tracks(region_tracks)

  return composite

def ChIPcomposite(assay):

  signals = dcc_data[(dcc_data.assay_type == "ChIP-seq") & (dcc_data.file_type == "BIGWIG") & (dcc_data.mapped_genome == "GRCz10/danRer10")].loc[:,"primary_file.name"].tolist()
  #signals = ["/".join([FILES_BASE, i]) for i in signals_basename]
  peaks = dcc_data[(dcc_data.assay_type == "ChIP-seq") & (dcc_data.loc[:,"primary_file.name"].str.contains("bigNarrowPeak") | dcc_data.loc[:,"primary_file.name"].str.contains("narrowPeak.bb")) & (dcc_data.mapped_genome == "GRCz10/danRer10")].loc[:,"primary_file.name"].tolist()
  #peaks = ["/".join([FILES_BASE, i]) for i in peaks_basename]

  chip_stages = []
  targets = []
  signal_tracks = []
  chip_seq_ids = []

  for bw in signals:

    file_name = bw.split('/')[-1]
    seq_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    data_id = str(dcc_data[dcc_data.loc[:,"primary_file.name"] == bw].data_id.tolist()[0])
    target = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_target'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    chip_stages.append(stage)
    chip_seq_ids.append(seq_id)
    targets.append(target)
    
    if stage == 'Prim-5':
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name="_".join([target, seq_id, data_id, "_signal"]),
        long_label=', '.join([target, stage, lab, tissue]),
        source="/".join([FILES_BASE,bw]),
        visibility=visibility,
        tracktype='bigWig',
        autoScale='on',
        color=trackhub.helpers.hex2rgb("#74C476"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage,'target':target,'kind':'signal', 'sample':seq_id, 'enstage':all_stages_enumerated[stage]},
      )

    signal_tracks.append(track)

  peak_tracks = []

  for bnp in peaks:

    file_name = bnp.split('/')[-1]
    seq_id = file_name.split('.')[1]
    data_id = str(dcc_data[dcc_data.loc[:,"primary_file.name"] == bnp].data_id.tolist()[0])
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    target = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_target'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    chip_seq_ids.append(seq_id)

    if stage == 'Prim-5':
      visibility = 'dense'
    else:
      visibility = 'hide'
    
    track = trackhub.Track(
        name="_".join([target, seq_id, data_id, "peaks"]),
        short_label = ', '.join([target, stage, lab, tissue, geo]),
        source="/".join([FILES_BASE, bnp]),
        visibility=visibility,
        tracktype='bigBed 6 4',
        autoScale='on',
        itemRgb='on',
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage,'target':target,'kind':'peak','sample':seq_id, 'enstage':all_stages_enumerated[stage]},
      )

    peak_tracks.append(track)
  
  composite = trackhub.CompositeTrack(
    name = 'ChIP-seqComposite',
    short_label='ChIP-seq tracks',
    tracktype='bed 3',
    sortOrder='enstage=+ sample=+ kind=-',
    dimensions="dimX=stage dimY=target dimA=kind dimB=sample dimC=enstage",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    ChIP-seq tracks
    ---------------

    This is a ChIP-seq super track that groups together signal (bigWig format) and narrow peaks (bigNarrowPeak format) of ChIP-seq data.
    regions (bigBed format).

    The ChIP-seq pipeline used to generate these tracks are described `here <https://gitlab.com/danio-code/DANIO-CODE_ChIP-seq>`.

    Briefly, the pipeline consists of:
      #. Aligning reads (bwa): Aligning raw reads to the reference genome.
      #. Filtering of aligned reads: Filtering the ba file for unaligned reads and and duplicates with samtools and sambamba.
      #. Optical duplicates detection: Detect optical duplicates with Picard tools.
      #. Duplicates removal: Second round of filtering with samtools and sambamba.
      #. Cross-correlation analysis.

    **Track definition**:
        ChIP-seq tracks consist of 2 different types:
        #. ChIP-seq signals.
        #. ChIP-seq narrow peaks.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ADANIO-CODE_RNA-seq>`.

    """
      ))

  subgroup_stages = [stage for stage in all_stages if stage in chip_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in chip_stages}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
        name='target',
        label='ChIP-seq_target',
        mapping=dict(zip(targets, targets))
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track_kind',
        mapping={'signal':'signal', 'peak':'peak'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(chip_seq_ids, chip_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')

  peak_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='peaks',
      visibility='dense',
      tracktype='bigBed 6 4',
      short_label='Peaks')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(peak_view)
  
  signal_view.add_tracks(signal_tracks)  
  peak_view.add_tracks(peak_tracks)

  return composite

def ChIPcomposite_dr11(assay):

  signals = dcc_data[(dcc_data.assay_type == "ChIP-seq") & (dcc_data.file_type == "BIGWIG") & (dcc_data.mapped_genome == "GRCz11/danRer11")].loc[:,"primary_file.name"].tolist()
  signals = ["/".join([FILES_BASE, i]) for i in signals]
  peaks = dcc_data[(dcc_data.assay_type == "ChIP-seq") & (dcc_data.loc[:,"primary_file.name"].str.contains("narrowPeak.bb")) & (dcc_data.mapped_genome == "GRCz11/danRer11")].loc[:,"primary_file.name"].tolist()
  peaks = ["/".join([FILES_BASE, i]) for i in peaks]

  chip_stages = []
  targets = []
  signal_tracks = []
  chip_seq_ids = []

  for bw in signals:

    file_name = bw.split('/')[-1]
    seq_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    target = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_target'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    chip_stages.append(stage)
    chip_seq_ids.append(seq_id)
    targets.append(target)
    
    if stage == 'Prim-5':
      visibility = 'full'
    else:
      visibility = 'hide'

    track = trackhub.Track(
        name=target + "_" + seq_id + "_signal",
        long_label=', '.join([target, stage, lab, tissue]),
        source=bw,
        visibility=visibility,
        tracktype='bigWig',
        autoScale='on',
        color=trackhub.helpers.hex2rgb("#74C476"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage,'target':target,'kind':'signal', 'sample':seq_id, 'enstage':all_stages_enumerated[stage]},
      )

    signal_tracks.append(track)

  peak_tracks = []

  for bnp in peaks:

    file_name = bnp.split('/')[-1]
    seq_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'stage'].tolist()[0]).replace(' ', '_')
    target = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_target'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['sequencing_id'] == seq_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    chip_seq_ids.append(seq_id)

    if stage == 'Prim-5':
      visibility = 'dense'
    else:
      visibility = 'hide'
    
    track = trackhub.Track(
        name=target + "_" + seq_id + "_peak",
        short_label = ', '.join([target, stage, lab, tissue, geo]),
        source=bnp,
        visibility=visibility,
        tracktype='bigBed 6 4',
        autoScale='on',
        itemRgb='on',
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage,'target':target,'kind':'peak','sample':seq_id, 'enstage':all_stages_enumerated[stage]},
      )

    peak_tracks.append(track)
  
  composite = trackhub.CompositeTrack(
    name = 'ChIP-seqComposite',
    short_label='ChIP-seq tracks',
    tracktype='bed 3',
    sortOrder='enstage=+ sample=+ kind=-',
    dimensions="dimX=stage dimY=target dimA=kind dimB=sample dimC=enstage",
    filterComposite="dimA dimB dimC",
    html_string=dedent(
    """
    ChIP-seq tracks
    ---------------

    This is a ChIP-seq super track that groups together signal (bigWig format) and narrow peaks (bigNarrowPeak format) of ChIP-seq data.
    regions (bigBed format).

    The ChIP-seq pipeline used to generate these tracks are described `here <https://gitlab.com/danio-code/DANIO-CODE_ChIP-seq>`.

    Briefly, the pipeline consists of:
      #. Aligning reads (bwa): Aligning raw reads to the reference genome.
      #. Filtering of aligned reads: Filtering the ba file for unaligned reads and and duplicates with samtools and sambamba.
      #. Optical duplicates detection: Detect optical duplicates with Picard tools.
      #. Duplicates removal: Second round of filtering with samtools and sambamba.
      #. Cross-correlation analysis.

    **Track definition**:
        ChIP-seq tracks consist of 2 different types:
        #. ChIP-seq signals.
        #. ChIP-seq narrow peaks.

    For more information on how the data were processed, please refer `here <https://gitlab.com/danio-code/ADANIO-CODE_RNA-seq>`.

    """
      ))

  subgroup_stages = [stage for stage in all_stages if stage in chip_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in chip_stages}   

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
    trackhub.SubGroupDefinition(
        name='target',
        label='ChIP-seq_target',
        mapping=dict(zip(targets, targets))
    ),
    trackhub.SubGroupDefinition(
        name='kind',
        label='Track_kind',
        mapping={'signal':'signal', 'peak':'peak'}
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(chip_seq_ids, chip_seq_ids))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')

  peak_view = trackhub.ViewTrack(
      name=assay+'regionsviewtrack',
      view='peaks',
      visibility='dense',
      tracktype='bigBed 6 4',
      short_label='Peaks')

  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  composite.add_view(peak_view)
  
  signal_view.add_tracks(signal_tracks)  
  peak_view.add_tracks(peak_tracks)

  return composite

def MNaseComposite(assay):

  signals = glob.glob('/zfin/danio-code/non_annotated_files/MNase-seq_tracks/*.bigwig')

  mnase_stages = []
  signal_tracks = []
  mnase_bios_ids = []

  for bw in signals:

    file_name = bw.split('/')[-1]
    bios_id = file_name.split('.')[1]
    stage = str(dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'stage'].tolist()[0]).replace(' ', '_')
    lab = dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'assay_lab'].tolist()[0].replace(' ', '_')
    tissue = str(dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'anatomical_term'].tolist()[0]).replace(' ', '_')
    geo = str(dcc_data.loc[dcc_data['biosample_id'] == bios_id, 'sra_geo_id'].tolist()[0]).replace(' ', '_')

    mnase_stages.append(stage)
    
    track = trackhub.Track(
        name=bios_id + "_signal",
        short_label=', '.join([stage, lab, tissue, geo]),
        source=bw,
        visibility='full',
        tracktype='bigWig',
        autoScale='on',
        color=trackhub.helpers.hex2rgb("#BAE4B3"),
        maxHeightPixels='128:32:8',
        subgroups={'stage':stage},
      )

    signal_tracks.append(track)

  composite = trackhub.CompositeTrack(
    name = 'MNase-seqComposite',
    short_label='MNase-seq tracks',
    tracktype='bigWig',
    dimensions="dimX=stage",
    html_string=dedent(
      """
      MNase-seq tracks consist of bigWig files containing negative logarithm of p-value representing the probability of observed signal over expected.
      """
      ))

  subgroup_stages = [stage for stage in all_stages if stage in mnase_stages]
  stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=stages_for_track,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'signalviewtrack',
      view='signal',
      visibility='full',
      tracktype='bigWig',
      short_label='Signal')
  
  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  
  signal_view.add_tracks(signal_tracks)  

  return composite

def HiCComposite(assay):

  hic_tracks = dcc_data[(dcc_data.assay_type == "Hi-C") & (dcc_data.file_type == "BIGWIG") & (dcc_data.mapped_genome == "GRCz10/danRer10")]
  type_mapping = {"di": "directionality index", "ii": "insulation index"}
  tracks = []

  for index, track in hic_tracks.iterrows():

    file_name = track['primary_file.name']
    res = file_name.split("_", 1)[1].split("_")[1].split(".")[0]
    track_type = file_name.split("_", 1)[1].split(".")[1]

    track = trackhub.Track(
        name="_".join(track[['sequencing_id', 'data_id']].tolist() + [res, track_type]),
        short_label = ', '.join(track[['stage', 'assay_lab', 'anatomical_term', 'sra_geo_id']].dropna().tolist() + [res, type_mapping[track_type]]),
        source="/".join([FILES_BASE, file_name]),
        visibility="full" if track.stage == "Prim-5" else "hide",
        tracktype='bigWig',
        autoScale='on',
        itemRgb='on',
        maxHeightPixels='128:32:8',
        subgroups={'stage':track.stage,'resolution':res,'type':track_type,'sample':track.sequencing_id, 'enstage':all_stages_enumerated[track.stage]},
      ) 
    
    track.add_params(color = '255,0,0', altColor = '0,0,255')


    tracks.append(track)

  composite = trackhub.CompositeTrack(
    name = 'HiC_Composite',
    short_label='HiC tracks',
    tracktype='bigWig',
    dimensions="dimX=stage dimY=resolution dimA=type dimB=sample dimC=enstage",
    sortOrder = "enstage=+ sample=+",
    filterComposite = "dimA dimB dimC",
    html_string=dedent(
      """
      HiC tracks consist of bigWig files containing insulation (Crane et al., 2015) and directionality index (Dixon et al., 2012) with varying window sizes (Kruse et al., 2016) of 50k, 500k, and 1Mb respectively.
      """
      )
    )

  hic_stages = set(hic_tracks.stage.replace(" ", "_"))
  subgroup_stages = OrderedDict([(stage, stage) for stage in all_stages if stage in hic_stages])
  #stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in hic_stages}

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=subgroup_stages,
    ),
    trackhub.SubGroupDefinition(
        name='resolution',
        label='Window size for index calculation',
        mapping={"50000":"50kb", "500000":"500kb", "1000000":"1MB"},
    ),
    trackhub.SubGroupDefinition(
        name='type',
        label='Index type',
        mapping=type_mapping,
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(set(hic_tracks.sequencing_id), set(hic_tracks.sequencing_id)))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'_bigWig',
      view='signal',
      visibility='hide',
      tracktype='bigWig',
      short_label='Signal')

  #signal_view.add_params(alt_color = '0,0,255')
  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  signal_view.add_tracks(tracks)
  
  print("danRer10")
  return composite

def HiCComposite_dr11(assay):

  hic_tracks = dcc_data[(dcc_data.assay_type == "Hi-C") & (dcc_data.file_type == "BIGWIG") & (dcc_data.mapped_genome == "GRCz11/danRer11")]
  type_mapping = {"di": "directionality index", "ii": "insulation index"}
  tracks = []

  for index, track in hic_tracks.iterrows():

    file_name = track['primary_file.name']
    res = file_name.split("_", 1)[1].split("_")[1].split(".")[0]
    track_type = file_name.split("_", 1)[1].split(".")[1]

    track = trackhub.Track(
        name="_".join(track[['sequencing_id', 'data_id']].tolist() + [res, track_type]),
        short_label = ', '.join(track[['stage', 'assay_lab', 'anatomical_term', 'sra_geo_id']].dropna().tolist() + [res, type_mapping[track_type]]),
        source="/".join([FILES_BASE, file_name]),
        visibility="full" if track.stage == "Prim-5" else "hide",
        tracktype='bigWig',
        autoScale='on',
        itemRgb='on',
        maxHeightPixels='128:32:8',
        subgroups={'stage':track.stage,'resolution':res,'type':track_type,'sample':track.sequencing_id, 'enstage':all_stages_enumerated[track.stage]},
      ) 
    
    track.add_params(color = '255,0,0', altColor = '0,0,255')


    tracks.append(track)

  composite = trackhub.CompositeTrack(
    name = 'HiC_Composite',
    short_label='HiC tracks',
    tracktype='bigWig',
    dimensions="dimX=stage dimY=resolution dimA=type dimB=sample dimC=enstage",
    sortOrder = "enstage=+ sample=+",
    filterComposite = "dimA dimB dimC",
    html_string=dedent(
      """
      HiC tracks consist of bigWig files containing insulation (Crane et al., 2015) and directionality index (Dixon et al., 2012) with varying window sizes (Kruse et al., 2016) of 50k, 500k, and 1Mb respectively.
      """
      )
    )

  hic_stages = set(hic_tracks.stage.replace(" ", "_"))
  subgroup_stages = OrderedDict([(stage, stage) for stage in all_stages if stage in hic_stages])
  #stages_for_track = OrderedDict([(stage, stage) for stage in subgroup_stages])
  en_stage_mapping = {all_stages_enumerated[i]:all_stages_enumerated[i] for i in hic_stages}

  subgroups = [
    trackhub.SubGroupDefinition(
        name='stage',
        label='Developmental_stage',
        mapping=subgroup_stages,
    ),
    trackhub.SubGroupDefinition(
        name='resolution',
        label='Window size for index calculation',
        mapping={"50000":"50kb", "500000":"500kb", "1000000":"1MB"},
    ),
    trackhub.SubGroupDefinition(
        name='type',
        label='Index type',
        mapping=type_mapping,
    ),
    trackhub.SubGroupDefinition(
      name='sample',
      label='Sequencing sample',
      mapping=dict(zip(set(hic_tracks.sequencing_id), set(hic_tracks.sequencing_id)))
    ),
    trackhub.SubGroupDefinition(
        name='enstage',
        label='Enumerated_stage',
        mapping=en_stage_mapping,
    ),
  ]

  signal_view = trackhub.ViewTrack(
      name=assay+'_bigWig',
      view='signal',
      visibility='hide',
      tracktype='bigWig',
      short_label='Signal')

  #signal_view.add_params(alt_color = '0,0,255')
  composite.add_subgroups(subgroups)
  composite.add_view(signal_view)
  signal_view.add_tracks(tracks)
  
  print("danRer11")
  return composite

# This will change when we will set up a new hub on a different server

FILES_BASE = '/zfin/danio-code/annotated_files'
TRACKHUB_LOC = '/zfin/danio-code/freezer/freeze-03'
GENOME = 'danRer10'
ASSAYS = ["BS-seq", "ATAC-seq", "RNA-seq", "CAGE-seq", "ChIP-seq", "MNase-seq", "HiC"]
ASSAYS_DANRER11 = ["RNA-seq", "CAGE-seq", "ChIP-seq", "3P-seq", "HiC"]
ASSAY_HUB={"BS-seq":BScomposite, "ATAC-seq":ATACcomposite, "RNA-seq": RNAcomposite, "CAGE-seq":CAGEcomposite, "ChIP-seq": ChIPcomposite, "MNase-seq":MNaseComposite, "HiC": HiCComposite}
ASSAY_HUB_DANRER11 = {"RNA-seq": RNAcomposite_dr11, "CAGE-seq":CAGEcomposite_dr11, "ChIP-seq": ChIPcomposite_dr11, "3P-seq":ThreeP_composite_dr11, "HiC": HiCComposite_dr11}

# Here we will create a new hub object

#hub, genomes_file, genome, trackdb = trackhub.default_hub(
#  hub_name='DANIO-CODE', # I think this is enough for the name, I suppose
#  short_label='DANIO-CODE Track Hub',
#  long_label='A Track Hub containing tracks produced with DANIO-CODE pipelines',
#  email='daniocode@gmail.com',
#  genome=GENOME
#  )

hub = Hub(hub = 'DANIO-CODE',
  short_label = 'DANIO-CODE Track Hub',
  long_label = 'A Track Hub containing tracks produced with DANIO-CODE pipelines',
  email = 'daniocode@gmail.com')

genome_danRer10 = Genome('danRer10')
genome_danRer11 = Genome('danRer11')
genomes_file = GenomesFile()
trackdb_danRer10 = TrackDb()
trackdb_danRer11 = TrackDb()
hub.add_genomes_file(genomes_file)
genomes_file.add_genome(genome_danRer10)
genomes_file.add_genome(genome_danRer11)
genome_danRer10.add_trackdb(trackdb_danRer10)
genome_danRer11.add_trackdb(trackdb_danRer11)


#hub.url =  "https://danio-code.zfin.org/trackhub/DANIO-CODE.hub.txt" DEPRACTED
#hub.remote_fn = "/zfin/danio-code/trackhub/DANIO-CODE.hub.txt"

assert trackdb_danRer10 is hub.genomes_file.genomes[0].trackdb
assert trackdb_danRer11 is hub.genomes_file.genomes[1].trackdb

#track = Track(
#   name='testTrack',
#   tracktype='bigWig',
#   short_label='test track',
#   long_label='track for testing purposes',
#   source='ATAC_test_1M_atac.bam_nodup.monoNsome.bw',
#   color='128,0,0'
# )

stages_dict = dict(zip(dcc_data.stage.unique(), dcc_data.stage.unique()))

for assay in ASSAYS:

  trackdb_danRer10.add_tracks(ASSAY_HUB[assay](assay))

for assay in ASSAYS_DANRER11:

  #print(assay)
  trackdb_danRer11.add_tracks(ASSAY_HUB_DANRER11[assay](assay))

#results = hub.render()

kwargs = dict(host = 'localhost', remote_dir = 'test_dir')
#upload_hub(hub=hub, **kwargs)
upload_hub(hub=hub, host='localhost', remote_dir=TRACKHUB_LOC)
#upload_track(track=track, **kwargs)
  
print 'Rendered!'
print 'Finished!'


  



    



